<?php
App::uses('AppShell', 'Console/Command');
App::uses('CakeEmail', 'Network/Email');

class QueueAdvancedEmailTask extends AppShell 
{
	/**
	 * @var boolean
	 */
	public $autoUnserialize = true;
	
	/**
	 * Example run function.
	 * This function is executed, when a worker is executing a task.
	 * The return parameter will determine, if the task will be marked completed, or be requeued.
	 *
	 * @param array $data The array passed to QueuedTask->createJob()
	 * @return boolean Success
	 */
	public function run($data) 
	{
		try {
			$this->hr();
			$this->out('Email sending for ' . implode(',' , $data['to']));
			$this->hr();
			$this->_send($data);
			$this->out(' ');
			$this->out(' ->Success, the Email Job was run.<-');
			$this->out(' ');
		} catch (Exception $e) {
			$this->out(' ->Error, the Email Job failed.<-');
			CakeLog::write('AdvancedEmail', 'fail');
		}
		return true;
	}
	
	/**
	* Send Mail
	* @param array $options
	* @return boolean
	*/
	private function _send($options=array()) 
	{
		$cakeEmail = new CakeEmail();
		$cakeEmail->template($options['template'], 'default')
			->config('smtp')
			->emailFormat('html')
			->to($options['to'])
			->subject($options['subject'])
			->viewVars(array('data' => $options['data']))
			->send();
			
		return true;
	}
}