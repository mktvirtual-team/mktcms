<?php
App::uses('AppModel', 'Model');

class Log extends AppModel
{
    const ACTION_ADD = 1;
    const ACTION_EDIT = 2;
    const ACTION_DELETE = 3;

    public static $actions = array(
        self::ACTION_ADD => "Adicionou",
        self::ACTION_EDIT => "Editou",
        self::ACTION_DELETE => "Excluiu"
    );

    /**
     * Nome do modelo em português, usado nas views
     * @var string
     */
    public static $label = "Log";

    /**
     * Nome do modelo em português
     * @var type
     */
    public static $labels = array(
        "who" => "Quem",
        "action" => "Ação",
        "model" => "O que",
        "created" => "Data",
        "data" => "Informações",
        "model_id" => "Id do modelo",
        "user_id" => "Id do usuário",
        "plugin" => "Plugin"
    );

    public static function changes($model, $json)
    {
        $data = json_decode($json, true);

        $return = array();

        if (!is_array($data)) {
            $data = array();
        }

        foreach($data as $key => $value) {
            if (isset($model::$labels[$key])) {
                $label = $model::$labels[$key];
            } else {
                $label = $key;
            }

            $return[] .= sprintf("%s: %s", $label, $value);
        }

        return implode(", ", $return);
    }
}
