<?php
App::uses('AppModel', 'Model');

class Bar extends AppModel
{
    public $validate = array(
        'name' => array(
            'rule' => array('minLength', '3'),
            'message' => 'Este campo deve conter no minimo 3 caracteres'
        )
    );

    public $hasMany = array(
        "Foo" => array("dependent" => true),
        "OtherFoo" => array("dependent" => true, "foreignKey" => "other_bar_id", "className" => "Foo")
    );

    public $hasAndBelongsToMany = array("Foo");

    /**
     * Nome do modelo em português, usado nas views
     * @var string
     */
    public static $label = "Bar";
    public static $labels = array(
        "name" => "Nome do Bar"
    );
}
