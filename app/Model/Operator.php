<?php
App::uses('AppModel', 'Model');
App::uses('CakeEmail', 'Network/Email');

class Operator extends AppModel
{
    const TYPE_OPERATOR = 1;
    const TYPE_ADMINISTRATOR = 2;

    public static $types = array(
        self::TYPE_OPERATOR => 'Operador',
        self::TYPE_ADMINISTRATOR => 'Administrador',
    );
    
    public static $unprotected_types = array(
        self::TYPE_OPERATOR => 'Operador',
    );
    
    public $validate = array(
        'name' => array(
            'rule' => 'notEmpty',
            'message' => 'O nome deve ser preenchido'
        ),
        'email' => array(
            'mustUnique' => array(
                'rule' => 'isUnique',
                'required' => true,
                'message' => 'E-mail já cadastrado!',
                'last' => true
            ),
            'mustNotEmpty' => array(
                'rule' => 'notEmpty',
                'allowEmpty' => false,
                'message' => 'O e-mail deve ser preenchido!',
                'last' => true
            ),
            'mustBeEmail' => array(
                'rule' => array('email'),
                'message' => 'E-mail inválido',
                'last' => true
            )
        ),
        'password' => array(
            'mustNotEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'Senha é obrigatório!',
                'on' => 'create',
                'last' => true),
            'mustBeLonger' => array(
                'rule' => array('minLength', 6),
                'message' => 'Senha deve conter mais de 5 caracteres!',
                'last' => true
            ),
            'mustMatch' => array(
                'rule' => array('verifies'),
                'message' => 'Senha inválida, confirme a sua senha novamente'
            )
        )
    );

    /**
     * Nome do modelo em português, usado nas views
     * @var string
     */
    public static $label = 'Operador';

    /**
     * Nome dos campos em português , usados nas views (Forms,Grids)
     * @var array 
     */
    public static $labels = array(
        "email" => "E-mail",
        "name" => "Nome",
        "password" => "Senha",
        "cpassword" => "Confirmar senha",
        "type" => "Tipo",
        "active" => "Ativo"
    );

    public function beforeValidate($options = array())
    {
        parent::beforeValidate($options);
		
        if (isset($this->data[$this->alias]['id']) && empty($this->data[$this->alias]['password'])) {
            unset($this->data[$this->alias]['password']);
        }
    }

    public function beforeSave($opt = array())
    {
        if (isset($this->data[$this->alias]['password']) && !empty($this->data[$this->alias]['password'])) {
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
        }

        return true;
    }

    public function verifies()
    {
        if (!isset($this->data[$this->alias]['cpassword'])) {
            return false;
        }

        return ($this->data[$this->alias]['password'] === $this->data[$this->alias]['cpassword']);
    }

    public function sendResetPassword($operatorId)
    {
        $this->id = $operatorId;
        $operator = $this->read();

        if (empty($operator['Operator']['hash'])) {
            return false;
        }

        $this->_queueEmail(array(
            'to' => array($operator['Operator']['email']),
            'subject' => 'Redefinir senha',
            'template' => 'reset',
            'data' => $operator['Operator']
        ));

        return true;
    }

    public static function getTypes()
    {
        if ( self::_getAuthUser('type') == Operator::TYPE_OPERATOR) {
            return self::$unprotected_types;
        } else {
            return self::$types;
        }
    }
}