<div class="col-md-12">
	<div class="col-md-4">
		<h2>Criar tabelas</h2>
		<p>Aqui você pode visualizar o dump do banco de dados da sua aplicação!</p>
		<p><?php echo $this->Html->link('Visualizar Schema', '/mkt_migrate/migrates/view', array('class' => 'btn btn-primary', 'role' => 'button')) ?></p>
	</div>

	<div class="col-md-4">
		<h2>Config DB</h2>
		<p>Aqui é possível criar o arquivo de configuração do banco de dados!</p>
		<p><?php echo $this->Html->link('Configurar DB', '/mkt_migrate/migrates/config', array('class' => 'btn btn-primary', 'role' => 'button')) ?></p>
	</div>

	<div class="col-md-4">
		<h2>.HTACCESS</h2>
		<p>Aqui é possível alterar o RewriteBase do htaccess</p>
		<p><?php echo $this->Html->link('Configurar .htaccess', '/mkt_migrate/migrates/rewrite', array('class' => 'btn btn-primary', 'role' => 'button')) ?></p>
	</div>
</div>

<div class="col-md-12">
	<div class="col-md-4 clear">
		<h2>Seed</h2>
		<p>Aqui é possível rodar o seed para popular o banco de dados!</p>
		<p><?php echo $this->Html->link('Rodar o Seed', '/mkt_migrate/migrates/seed/DevSeed', array('class' => 'btn btn-primary', 'role' => 'button')) ?></p>
	</div>

	<div class="col-md-4">
		<h2>Queue</h2>
		<p>Aqui é possível rodar a fila de tarefas do plugin Queue!</p>
		<p><?php echo $this->Html->link('Rodar a Fila', '/mkt_migrate/migrates/queue', array('class' => 'btn btn-primary', 'role' => 'button')) ?></p>
	</div>

	<div class="col-md-4">
		<h2>Config DEBUG</h2>
		<p>Aqui é possível alterar qualquer configuração no arquivo core.php!</p>
		<p><?php echo $this->Html->link('Configurar', '/mkt_migrate/migrates/core', array('class' => 'btn btn-primary', 'role' => 'button')) ?></p>
	</div>
</div>