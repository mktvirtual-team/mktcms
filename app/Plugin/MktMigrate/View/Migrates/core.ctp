<div class="col-md-4">
	<p>
		<div class="alert heading heading-mkt">
			<strong class="alert-heading">core.php</strong>
		</div>
	
		<textarea data-role="editor" name="revision_files" cols="50"><?php echo trim(file_get_contents(APP . 'Config' . DS . 'core.php')) ?></textarea>
		<button class="saveFile">Save</button>
		<?php echo $this->Html->scriptBlock("
			jQuery( function() {
				jQuery(window).bind('load', function() {
					var myCodeMirror = CodeMirror.fromTextArea(document.getElementsByTagName('textarea')[0], {
						mode: 'text/x-mysql',
						tabMode: 'indent',
						matchBrackets: true,
						autoClearEmptyLines: true,
						lineNumbers: true,
						theme: 'default'
					});
					jQuery('.saveFile').click(function(){
						var content = myCodeMirror.getValue();
						var path = jQuery('#hiddenFilePath').text();
						var response = confirm('Deseja realmente salvar este arquivo?');
						if(response) {
							$.ajax({
								type: 'POST',
								url: 'core',
								data: {c:content,p:path},
								dataType: 'text',
								success: function(){
									alert('Arquivo salvo');
								}
							});
						} else {
							alert('File not saved!');
						}
					});
				});
			});
		", array('inline' => false)); ?>
	
	</p>
</div>