<div class="panel panel-default">
    <div class="panel-heading">Pesquisar</div>
    <div class="panel-body">

        <?php $mdl = ClassRegistry::init($model); ?>

        <?php echo $this->BootstrapForm->create(false, array('type' => 'get', 'novalidate' => true)); ?>

        <?php echo $this->BootstrapForm->input('jobtype', array("type" => "select", "options" => $model::$types, "empty" => "Selecione")); ?>
        <?php echo $this->BootstrapForm->input('data'); ?>
        
        <div>
            <?php echo $this->Form->button('Limpar', array('class' => 'btn btn-default pull-left btn-clear-form', 'type' => 'button')); ?>
            <?php echo $this->Form->submit('Procurar', array('class' => 'btn btn-primary pull-right')); ?>
        </div>

        <?php echo $this->Form->end(); ?>

    </div>
</div>