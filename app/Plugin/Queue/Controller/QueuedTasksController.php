<?php

App::uses('QueueAppController', 'Queue.Controller');

class QueuedTasksController extends QueueAppController
{

    public $uses = array('Queue.QueuedTask');
    
    public $paginate = array(
        'limit' => 15,
    );

    /**
     * QueueController::beforeFilter()
     *
     * @return void
     */
    public function beforeFilter()
    {
        parent::beforeFilter();
        
        $this->QueuedTask->initConfig();

        $this->grid = array(
            array("field" => "QueuedTask.jobtype","format"=>QueuedTask::$types),
            array("field" => "QueuedTask.fetched"),
            array("field" => "QueuedTask.completed"),
            array("field" => "QueuedTask.failed"),
        );
        
    }
    
    /**
     * Run queue worker from web
     */
    public function admin_run()
    {
        App::import("Config","WebQueue");
        $queue = new WebQueue();
        $queue->initialize();
        $queue->loadTasks();
        $queue->runworker();
        
        $this->showSuccess(array("action"=>"index"));
    }

    public function admin_index()
    {

        $options = array(
            "name" => array("operation" => "contain"),
            "data" => array("operation" => "contain"),
        );

        $this->paginate['conditions'] = SearchComponent::createConditions($this->request, $options);
        $this->paginate['order'] = SearchComponent::createOrder($this->request, array('QueuedTask.jobtype' => 'asc'));

        $this->set('grid', $this->grid);

        if (!isset($this->params->named["export"])) {
            $this->set('result', $this->paginate($this->model));
        } else {
            $this->set('result', ClassRegistry::init($this->model)->find('all', $this->paginate));
            $this->render("/Common/admin_export");
        }
        
    }
    

    /**
     * Admin center.
     * Manage queues from admin backend (without the need to open ssh console window).
     *
     * @return void
     */
    public function admin_index_old()
    {

        $status = $this->_status();

        $current = $this->QueuedTask->getLength();
        $data = $this->QueuedTask->getStats();

        $this->set(compact('current', 'data', 'status'));
    }

    /**
     * Truncate the queue list / table.
     *
     * @return void
     */
    public function admin_reset()
    {
        if (!$this->Common->isPosted()) {
            throw new MethodNotAllowedException();
        }
        $res = $this->QueuedTask->truncate();
        if ($res) {
            $this->Common->flashMessage('OK', 'success');
        } else {
            $this->Common->flashMessage(__('Error'), 'success');
        }
        return $this->Common->autoPostRedirect(array('action' => 'index'));
    }

    /**
     * QueueController::_status()
     *
     * If pid loggin is enabled, will return an array with
     * - time: int Timestamp
     * - workers: int Count of currently running workers
     *
     * @return array Status array
     */
    protected function _status()
    {
        if (!($pidFilePath = Configure::read('Queue.pidfilepath'))) {
            return array();
        }
        $file = $pidFilePath . 'queue.pid';
        if (!file_exists($file)) {
            return array();
        }

        $sleepTime = Configure::read('Queue.sleeptime');
        $thresholdTime = time() - $sleepTime;
        $count = 0;
        foreach (glob($pidFilePath . 'queue_*.pid') as $filename) {
            $time = filemtime($filename);
            if ($time >= $thresholdTime) {
                $count++;
            }
        }

        $res = array(
            'time' => filemtime($file),
            'workers' => $count,
        );
        return $res;
    }

}
