<?php
/**
* Database logger
* @author Nick Baker 
* @version 1.0
* @license MIT

# Setup

in app/config/bootstrap.php add the following

CakeLog::config('database', array(
	'engine' => 'DatabaseLogger.DatabaseLogger',
	'model' => 'CustomLogModel' //'DatabaseLogger.Log' by default
));

*/
App::uses('ClassRegistry', 'Utility');
App::uses('CakeLogInterface','Log');
App::uses('ErrorLog', 'DatabaseLogger.Model');
class DatabaseLog implements CakeLogInterface{
	
	/**
	* Model name placeholder
	*/
	var $model = null;
	
	/**
	* Model object placeholder
	*/
	var $ErrorLog = null;
	
	/**
	 * Contruct the model class
	 */
	function __construct($options = array()){
		$this->model = isset($options['model']) ? $options['model'] : 'DatabaseLogger.ErrorLog';
		$this->ErrorLog = ClassRegistry::init($this->model);
	}
	
	/**
	 * Write the log to database
	 */
	public function write($type, $message) {
		try{
                $this->ErrorLog->create();
		$this->ErrorLog->save(array(
			'type' => $type,
			'message' => $message
		));
                }
                catch(Exception $e)
                {
                    
                }
	}
}
