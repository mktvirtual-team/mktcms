<?php echo $this->BootstrapForm->create($model, array('type' => 'file', 'novalidate' => true)); ?>

<div class="row">
    <div class="col-md-3">

        <?php echo $this->BootstrapForm->input('id', array('type' => 'hidden')); ?>

        <div class="panel panel-default">
            <div class="panel-heading">Geral</div>

            <div class="panel-body">

                <?php echo $this->BootstrapForm->input('email'); ?>
                <?php echo $this->BootstrapForm->input('name'); ?>
                <?php echo $this->BootstrapForm->input('surname'); ?>
                <?php echo $this->BootstrapForm->input('phone',array('class'=>'format-phone')); ?>
                
            </div>

        </div>

    </div>

</div>

<nav class="navbar navbar-default navbar-fixed-bottom" role="navigation">
    <div class="container-fluid">

        <?php echo $this->Form->button('<span class="glyphicon glyphicon-ok"></span>  Salvar', array('class' => 'btn btn-success pull-left navbar-btn')); ?>

        <?php if (isset($this->data[$model]["id"])): ?>

            <?php echo $this->Html->link('<span class="glyphicon glyphicon-trash"></span> Excluir', array("action" => "delete", $this->data[$model]["id"]), array('class' => 'btn btn-danger pull-right navbar-btn action-delete', 'escape' => false)); ?>

        <?php endif ?>

    </div>
</nav>

<?php echo $this->Form->end(); ?>
