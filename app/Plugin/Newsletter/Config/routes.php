<?php

Router::connect('/admin/newsletter/:action/*', array(
    'admin'      => true, 
    'prefix'     => 'admin',
    'plugin'     => 'newsletter', 
    'controller' => 'newsletters'
));

Router::connect('/admin/newsletter', array(
    'admin'      => true, 
    'prefix'     => 'admin',
    'plugin'     => 'newsletter', 
    'controller' => 'newsletters'
));