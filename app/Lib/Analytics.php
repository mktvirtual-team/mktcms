<?php

class Analytics
{
    static $keys = array();
    
    public static function trackEvent($keys = array())
    {
        self::$keys = array_merge(self::$keys,$keys);
    }
    
    public static function renderTrackEventScript($view)
    {
        if(empty(self::$keys))
        {
            return "";
        }
        
        $script = "_gaq.push(['_trackEvent','".implode("','", self::$keys)."']);";
        
        if(Configure::read("debug"))
        {
            CakeLog::write("_trackEvent",implode(",", self::$keys));
        }
        
        return $view->Html->scriptBlock($script);
        
    }
    
}

?>
