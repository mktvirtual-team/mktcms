<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">

        <div class="panel panel-default">
            <div class="panel-heading"><?php echo Configure::read("Application.title");?> - Criar nova senha</div>
            <div class="panel-body">

                <?php $mdl = ClassRegistry::init($model); ?>

                <?php echo $this->BootstrapForm->create($model, array('type' => 'post', 'novalidate' => true)); ?>

				<?php echo $this->BootstrapForm->input('password'); ?>
				<?php echo $this->BootstrapForm->input('cpassword', array('type' => 'password')); ?>

                <?php echo $this->Form->button('<span class="glyphicon glyphicon-ok"></span>  Enviar', array('class' => 'btn btn-success pull-left navbar-btn')); ?>
				
                <?php echo $this->Form->end(); ?>

            </div>

        </div>

        <?php echo $this->Session->flash(); ?>
        
    </div>
    <div class="col-md-4"></div>
    
</div>
