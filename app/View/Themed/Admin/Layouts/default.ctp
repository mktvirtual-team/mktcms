<!DOCTYPE html>
<html>
    <head>
        
        <?php echo $this->element('Layout/default/head'); ?>
        
    </head>
    
    <body>
        
        <?php echo $this->element('Layout/default/dialogs'); ?>
        <?php echo $this->element('Layout/default/menu'); ?>                                                                                                                                                                                                                                                        

        <div class="container-fluid">
            
            <?php echo $this->Session->flash(); ?>
            <?php echo $this->fetch('breadcrumb'); ?>
            <?php echo $this->fetch('content'); ?>
        
        </div>
        
        <?php echo $this->element('Layout/default/js'); ?>
        <?php echo $this->element('sql_dump'); ?>
        
    </body>
    
</html>
