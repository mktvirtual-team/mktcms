<div class="panel panel-default">
    <div class="panel-heading" data-toggle="collapse" data-target="#search_pane">Pesquisar<span class="glyphicon glyphicon-resize-vertical pull-right"></span></div>
    <div class="panel-body collapse in" id="search_pane">

        <?php echo $this->fetch('content') ?>

    </div>
</div>