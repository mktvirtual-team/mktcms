<div class="exportable">

    <?php foreach ($grid as $data): ?>

        <?php
        echo $this->Form->input($data["field"], array(
            "type" => "checkbox",
            "label" => $this->Grid->getFieldLabel($data),
            "class" => "field",
            "data-field" => $data["field"],
            "checked" => $this->Grid->fieldIsSelected($data["field"])
        ));
        ?>

    <?php endforeach; ?>

</div>
