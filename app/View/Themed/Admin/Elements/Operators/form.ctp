<?php echo $this->BootstrapForm->create($model, array('type' => 'file', 'novalidate' => true)); ?>
<?php echo $this->BootstrapForm->input('id', array('type' => 'hidden')); ?>

<div class="row">
    <div class="col-md-3">

        <div class="panel panel-default">
            <div class="panel-heading">Geral</div>

            <div class="panel-body">
                
                <?php echo $this->BootstrapForm->input('email'); ?>
                <?php echo $this->BootstrapForm->input('name'); ?>
                <?php echo $this->BootstrapForm->input('password'); ?>
                <?php echo $this->BootstrapForm->input('cpassword',array("type"=>"password")); ?>
                <?php echo $this->BootstrapForm->input('type', array("type" => "select", "options" => $model::$types, "empty" => "Selecione")); ?> 
                <?php echo $this->BootstrapForm->checkbox('active'); ?>
                

            </div>

        </div>

    </div>


</div>

<?php echo $this->element('../Common/form_footer'); ?>

<?php echo $this->Form->end(); ?>
