<?php
$menu = $this->Menu;
ClassRegistry::init("Operator");
?>

<nav class="navbar navbar-default navbar-fixed-top" role="navigation">

    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-6">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo Router::url("/admin", true) ?>"><?php echo Configure::read("Application.title");?></a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-6">

            <ul class="nav navbar-nav">

                <?php echo $menu->item('Home', array('controller' => 'home'), array('home')); ?>

                <?php
                echo $menu->dropdown('Foos', array('foos', 'bars'), function() use ($menu) {

                            $return = $menu->item('Listar', array('controller' => 'foos', 'action' => 'index'));
                            $return .= $menu->item('Novo', array('controller' => 'foos', 'action' => 'add'));
                            $return .= $menu->divider();
                            $return .= $menu->item('Bars', array('controller' => 'bars', 'action' => 'index'));
                            return $return;
                        });
                ?>

                <?php
                echo $menu->dropdown('Operadores', array('operators'), function() use ($menu) {

                            $return = $menu->item('Listar', array('controller' => 'operators', 'action' => 'index'));
                            $return .= $menu->item('Novo', array('controller' => 'operators', 'action' => 'add'));
                            return $return;
                        });
                ?>

                <?php
                echo $menu->dropdown('Ferramentas', array('logs', 'errorLogs','queuedTasks'), function() use ($menu) {

                            $return = $menu->item('Log', array('controller' => 'logs'), false, array(Operator::TYPE_ADMINISTRATOR));
                            $return .= $menu->item('ErrorLog', array('controller' => 'errorLogs'), false, array(Operator::TYPE_ADMINISTRATOR));
                            $return .= $menu->divider();
                            $return .= $menu->item('Tarefas', array('controller' => 'queuedTasks', 'plugin' => 'queue'), false, array(Operator::TYPE_ADMINISTRATOR));

                            return $return;
                        }, array(Operator::TYPE_ADMINISTRATOR));
                ?>

                <?php
                echo $menu->dropdown('Plugins', array('newsletters'), function() use ($menu) {

                            $return = $menu->item('Newsletters', array('controller' => 'newsletters','plugin' => 'newsletter'));
                            return $return;
                        });
                ?>

            </ul>

            <ul class="nav navbar-nav navbar-right">

                <?php
                echo $menu->dropdown(sprintf("Olá, %s", AuthComponent::user('name')), array('config'), function() use ($menu) {

                            $return = $menu->item('Editar meus dados', array('controller' => 'operators', 'action' => 'edit', AuthComponent::user('id')));
                            $return .= $menu->divider();
                            $return .= $menu->item('Configurações', array('controller' => 'configs'));
                            $return .= $menu->divider();
                            $return .= $menu->item('Sair', array('controller' => 'operators', 'action' => 'logout'));

                            return $return;
                        });
                ?>

            </ul>

        </div>
    </div>


</nav>

<?php $menu->autoGenerate(); ?>