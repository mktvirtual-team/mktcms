<?php
$vars = sprintf("var URL_BASE = '%s';", Router::url("/", true));
$vars .= sprintf("var CONTROLLER = '%s';", $this->params->controller);
$vars .= sprintf("var PLUGIN = '%s';", "" . $this->params->plugin);

echo $this->Html->scriptBlock($vars);

$jsList = array(
    'vendor/jquery-1.11.1.min',
    'vendor/jquery-ui.min',
    //'vendor/jquery-migrate-1.2.1',
    'vendor/bootstrap.min',
    'vendor/autoNumeric',
    'vendor/jquery.maskedinput-1.3',
    'vendor/jquery.form.min',
    'vendor/jquery.cookie',
    'vendor/bootstrap-multiselect',
    'vendor/bootstrap-typeahead',
    //'vendor/ckeditor/ckeditor.js',
    //'vendor/ckeditor/adapters/jquery.js',
    'vendor/bootstrap3-wysihtml5.all.min',
    'vendor/jquery.dragscroll.js',
    //'vendor/mindmap.js',
    'vendor/extend',
	'vendor/select2.min',
    'app',
    'app.form',
    'app.dialog',
    'app.autocomplete',
    'app.grid',
    'app.list',
    'app.counter',
	'app.sort'
);

if (isset($this->viewVars['requestJs']))
    $jsList[] = $this->viewVars['requestJs'];

// Script para a view aual
if (file_exists(WWW_ROOT . "theme/admin/js/". CURRENT_VIEW . '.js'))
    $jsList[] = "/theme/admin/js/". CURRENT_VIEW . '.js';

// Script para todo o controller
if (file_exists(WWW_ROOT . "theme/admin/js/". $this->params['controller'] . '/default.js'))
    $jsList[] = "/theme/admin/js/". $this->params['controller'] . '/default.js';

?>

<?php echo $this->fetch('script'); ?>
<?php echo $this->ScriptCombiner->js($jsList); ?>
<?php #echo $this->Html->script($jsList); ?>
<?php echo $this->Model->exportConsts(array("Foo.TYPE_1")); ?>
