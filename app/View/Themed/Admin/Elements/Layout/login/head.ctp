<?php echo $this->Html->charset(); ?>
<title><?php echo Configure::read("Application.title"); ?> | Painel de Controle</title>

<?php

if ($this->params['action'] != 'display'):
    define('CURRENT_VIEW', $this->params['controller'] . '/' . $this->params['action']);
else:
    define('CURRENT_VIEW', $this->params['controller'] . '/' . $this->params['pass'][0]);
endif;

$cssList = array('bootstrap.min', 'layout', 'animate.min');

if (isset($this->viewVars['requestCss'])):
    $cssList[] = $this->viewVars['requestCss'];
endif;

if (file_exists(CSS . CURRENT_VIEW . '.css')):
    $cssList[] = CURRENT_VIEW . '.css';
endif;

echo $this->Html->meta('icon');
echo $this->fetch('meta');
echo $this->fetch('css');

echo $this->ScriptCombiner->css($cssList);
?>
