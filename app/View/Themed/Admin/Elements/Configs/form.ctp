<?php echo $this->BootstrapForm->create($model, array('type' => 'file', 'novalidate' => true)); ?>

<div class="row">
    <div class="col-md-3">

        <?php echo $this->BootstrapForm->input('id', array('type' => 'hidden')); ?>
        
        <div class="panel panel-default">
            <div class="panel-heading">Geral</div>

            <div class="panel-body">
                
                
                <?php if(AuthComponent::user("type")==Operator::TYPE_ADMINISTRATOR): ?>
                
                    <?php echo $this->BootstrapForm->input('name'); ?>
                    <?php echo $this->BootstrapForm->input('label'); ?>
                    <?php echo $this->BootstrapForm->input('value'); ?>
                    <?php echo $this->BootstrapForm->checkbox('editable'); ?>
                    <?php echo $this->BootstrapForm->input('type', array("type" => "select", "options" => $model::$types, "empty" => "Selecione")); ?>
                
                <?php else: ?>

                    <?php echo $this->BootstrapForm->input('value',array('label' => $this->data["Config"]["label"])); ?>
                
                <?php endif; ?>
                
            </div>

        </div>

    </div>

</div>

<?php echo $this->element('../Common/form_footer'); ?>

<?php echo $this->Form->end(); ?>
