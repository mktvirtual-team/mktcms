<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger alert-dismissable animated flash">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <span class="glyphicon glyphicon-thumbs-down"></span> <?php echo $message; ?>
        </div>
    </div>
</div>
