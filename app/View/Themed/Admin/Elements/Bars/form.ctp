<?php echo $this->BootstrapForm->create($model, array('type' => 'file', 'novalidate' => true)); ?>

<div class="row">
    <div class="col-md-3">

        <?php echo $this->BootstrapForm->input('id', array('type' => 'hidden')); ?>

        <div class="panel panel-default">
            <div class="panel-heading">Geral</div>

            <div class="panel-body">

                <?php echo $this->BootstrapForm->input('name'); ?>

            </div>

        </div>

    </div>

</div>

<?php echo $this->element('../Common/form_footer'); ?>

<?php echo $this->Form->end(); ?>
