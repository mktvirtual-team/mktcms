<div class="row">
    <div class="col-md-12">
        <?php echo $this->Breadcrumb->drop("Home", array("controller" => "home"))->create(); ?>
    </div>
</div>

<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">

        <div class="panel panel-default">
            <div class="panel-heading">Resumo</div>

            <div class="panel-body">

                <?php
                ClassRegistry::init("Foo");

                echo $this->Counter->create(array(
                    array("label" => "Foos", "model" => "Foo"),
                    array("label" => "Bars", "model" => "Bar"),
                    array("label" => "Foos tipo 1", "model" => "Foo", "url" => array(
                            "controller" => "foos",
                            "?" => array("type" => Foo::TYPE_1)
                        ), array(
                            "type" => Foo::TYPE_1,
                        ))
                ));
                ?>   

            </div>

        </div>

    </div>    
</div>

