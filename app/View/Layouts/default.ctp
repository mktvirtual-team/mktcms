<?php echo $this->Html->docType('html5'); ?>
<html class="no-js">
    <head>
        <?php echo $this->element('Layout/head'); ?>
        <?php echo $this->Html->script('vendor/modernizr.custom'); ?>
    </head>

    <body>
        <div class="container">
            <?php echo $this->element('Layout/header'); ?>

            <section id="content">
                <?php echo $this->fetch('content'); ?>
            </section>

            <?php echo $this->element('Layout/footer'); ?>
        </div>
    </body>

    <?php echo $this->element('Layout/script') ?>

</html>