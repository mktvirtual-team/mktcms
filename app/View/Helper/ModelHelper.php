<?php
App::uses('AppHelper', 'View/Helper');

class ModelHelper extends AppHelper
{
    public $helpers = array("Html");

    /**
     * Exporta as constantes nos models para o javascript
     * @param array $consts Constantes a serem exportadas
     * @return string
     */
    public function exportConsts($consts = array())
    {
        $scripts = "";
        foreach (App::objects('Model') as $model) {
            if ($model != "AppModel") {
                $scripts .= self::exportConstsAsJavaScript($model, $consts);
            }
        }

        return $this->Html->scriptBlock($scripts);
    }

    /**
     * Esporta as constantes de um model especifico
     * @param type $class Nome do model
     * @param type $consts Constantes a serem exportadas
     * @return type
     */
    public static function exportConstsAsJavaScript($class, $consts)
    {
        App::uses($class, "Model");

        $refl = new ReflectionClass($class);

        $temp = "var $class = {};\n";

        foreach ($refl->getConstants() as $key => $value) {
            if (empty($consts) || in_array("$class.$key", $consts)) {
                $value = is_numeric($value) ? $value : "'$value'";
                $temp .= "$class.$key=$value;\n";
            }
        }

        return $temp;
    }
}
