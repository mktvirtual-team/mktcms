<?php
App::uses('AppHelper', 'View/Helper');
Configure::load('script_combiner');
App::import('Lib', 'Compressor');
App::import('Vendor', 'JsMin', array('file' => 'jsmin' . DS . 'jsmin.php'));

class ScriptCombinerHelper extends AppHelper
{
	public $helpers = array('Html', 'Js');
	private $cssCachePath;
	private $jsCachePath;
	private $cacheLength;
	private $enabled = false;

	/**
	 * Constructor
	 *
	 * @param View $View
	 * @param array $settings
	 */
	public function __construct(View $View, $settings = array())
	{
		parent::__construct($View, $settings);

		if (!Configure::read('ScriptCombiner')) {
			Configure::write('ScriptCombiner.combineJs', true);
			Configure::write('ScriptCombiner.combineCss', true);
			Configure::write('ScriptCombiner.compressJs', true);
			Configure::write('ScriptCombiner.compressCss', true);
			Configure::write('ScriptCombiner.cacheLength', 3600 * 24);
			Configure::write('ScriptCombiner.cssCachePath', CSS);
			Configure::write('ScriptCombiner.jsCachePath', JS);
			Configure::write('ScriptCombiner.fileSeparator', "\n\n/** FILE SEPARATOR **/\n\n");
		}

		$this->cssCachePath = Configure::read('ScriptCombiner.cssCachePath');
		if (!is_dir($this->cssCachePath)) {
			throw new Exception('Cannot locate CSS combination cache directory at ' . $this->cssCachePath . ', or path is not writable.');
		}

		$this->jsCachePath = Configure::read('ScriptCombiner.jsCachePath');
		if (!is_dir($this->jsCachePath) || !is_writable($this->jsCachePath)) {
			throw new Exception('Cannot locate Javascript combination cache directory at ' . $this->jsCachePath . ', or path is not writable.');
		}

		$cacheLength = Configure::read('ScriptCombiner.cacheLength');
		if (is_string($cacheLength)) {
			$this->cacheLength = strtotime($cacheLength) - time();
		} else {
			$this->cacheLength = (int)$cacheLength;
		}

		if ( Configure::read('debug') == 0 ) {
			$this->enabled = true;
		}
	}

	public function getRevision()
	{
		return Configure::read('ScriptCombiner.key');
	}

	/**
	 * Combine and Compress CSS files
	 *
	 * @return mixed
	 */
	public function css()
	{
		$cssFiles = func_get_args();

		if (empty($cssFiles)) {
			return '';
		}

		if (!$this->enabled || Configure::read('ScriptCombiner.combineCss') == false) {
			return $this->Html->css($cssFiles);
		}

		if (is_array($cssFiles[0])) {
			$cssFiles = $cssFiles[0];
		}

		$revision = $this->getRevision();
		$cacheKey = md5(serialize($cssFiles));
		$cacheFile = "{$this->cssCachePath}{$cacheKey}-{$revision}.css";

		if ($this->isCacheFileValid($cacheFile)) {
			return $this->Html->css($this->convertToUrl($cacheFile));
		}

		$cssData = $this->makeScriptCombinerAndCacheable($cssFiles, 'css');

		if (file_put_contents($cacheFile, $cssData) > 0) {
			return $this->Html->css($this->convertToUrl($cacheFile));
		}

		return $this->Html->css($cssFiles);
	}

	/**
	 * Combine and Compress JS files
	 *
	 * @return mixed
	 */
	public function js()
	{
		$jsFiles = func_get_args();

		if (empty($jsFiles)) {
			return '';
		}

		if (!$this->enabled || Configure::read('ScriptCombiner.combineJs') == false) {
			return $this->Html->script($jsFiles);
		}

		if (is_array($jsFiles[0])) {
			$jsFiles = $jsFiles[0];
		}

		$revision = $this->getRevision();
		$cacheKey = md5(serialize($jsFiles));
		$cacheFile = "{$this->jsCachePath}{$cacheKey}-{$revision}.js";

		if ($this->isCacheFileValid($cacheFile)) {
			return $this->Html->script($this->convertToUrl($cacheFile));
		}

		$jsData = $this->makeScriptCombinerAndCacheable($jsFiles, 'js');

		if (file_put_contents($cacheFile, $jsData) > 0) {
			return $this->Html->script($this->convertToUrl($cacheFile));
		}

		return $this->Html->script($jsFiles);
	}

	/**
	 * Make compress and combine scripts
	 *
	 * @param array $files
	 * @return mixed
	 */
	public function makeScriptCombinerAndCacheable($files, $type)
	{
		$scriptData = array();

		if ($type == 'js') {
			$links = $this->Html->script($files);
			preg_match_all('/src="([^"]+)"/i', $links, $urlMatches);
		} else {
			$links = $this->Html->css($files, 'import');
			preg_match_all('#\(([^\)]+)\)#i', $links, $urlMatches);
		}

		if (isset($urlMatches[1])) {
			$urlMatches = ($type == 'js') ? array_unique($urlMatches[1]) : $urlMatches[1];
		} else {
			$urlMatches = array();
		}

		foreach ($urlMatches as $urlMatch) :
			$path = str_replace(array('/', '\\'), DS, WWW_ROOT . ltrim(Router::normalize($urlMatch), '/'));

			if (!is_file($path)) {
				continue;
			}

			if ($type == 'css') {
				$cssTmp = file_get_contents($path);
				$scriptData[] = $this->rewriteUri($cssTmp, $path);
			} else {
				$scriptData[] = file_get_contents($path);
			}
		endforeach;

		$scriptData = implode(Configure::read('ScriptCombiner.fileSeparator'), $scriptData);

		$methodCompress = "compress" . ucfirst($type);
		if (Configure::read("ScriptCombiner.{$methodCompress}") && $this->enabled) {
			CakeLog::write('compress', "ScriptCombiner.{$methodCompress}");
			$scriptData = $this->$methodCompress($scriptData);
		}

		return $scriptData;
	}

	/**
	 * Verify expired file
	 *
	 * @param string
	 * @return bool
	 */
	private function isCacheFileValid($cacheFile)
	{
		if (is_file($cacheFile) && $this->cacheLength > 0) {
			$lastModified = filemtime($cacheFile);
			$timeNow = time();
			if (($timeNow - $lastModified) < $this->cacheLength) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Convert to URL
	 * Ex:
	 *   /htdocs/view/app/webroot/css/cache/combined.9afc4b1e.css
	 * TO
	 *   /css/cache/combined.9afc4b1e.css
	 *
	 * @param string $filePath
	 * @return string
	 */
	private function convertToUrl($filePath)
	{
		$___path = Set::filter(explode(DS, $filePath));
		$___root = Set::filter(explode(DS, WWW_ROOT));
		$webrootPaths = array_diff_assoc($___path, $___root);

		return ('/' . implode('/', $webrootPaths));
	}

	/**
	 * Rewrite URL css for correct document root server
	 *
	 * @param string $css
	 * @param string $path
	 * @return string
	 */
	private function rewriteUri($css, $path)
	{
		App::import('Vendor', 'Minify_CSS', array('file' => 'Minify/CSS.php'));

		if ($this->request->webroot == '/') {
			$prependRelativePath = '/';
		} else {
			$prependRelativePath = $this->request->webroot;
		}

		if (preg_match('/(theme\\/([^\\/]+))/', $path, $matches)) {
			$prependRelativePath .= $matches[0] . DS;
		}

		$options = array(
			'preserveComments' => false,
			'currentDir' => CSS,
			'docRoot' => WWW_ROOT,
			'prependRelativePath' => $prependRelativePath
		);

		return Minify_CSS::minify($css, $options);
	}

	/**
	 * Compress CSS with replace pattern
	 *
	 * @param array|string
	 * @return mixed
	 */
	private function compressCss($cssData)
	{
		$cssData = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $cssData);
		$cssData = str_replace(array("\r\n", "\r", "\n", "\t"), '', $cssData);
		$cssData = preg_replace('/;\s*}/i', '}', $cssData);
		$cssData = preg_replace('/[\t\s]*{[\t\s]*/i', '{', $cssData);
		$cssData = preg_replace('/[\t\s]*:[\t\s]*/i', ':', $cssData);
		$cssData = preg_replace('/(\d)[\s\t]+(em|px|%)/i', '$1$2', $cssData);
		$cssData = preg_replace('/url[\s\t]+\(/i', 'url(', $cssData);

		return $cssData;
	}

	/**
	 * Compress JS with JSMIN
	 *
	 * @param string $jsData
	 * @return string
	 */
	private function compressJs($jsData)
	{
		return JsMin::minify($jsData);
	}
}
