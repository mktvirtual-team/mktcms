<?php
App::uses('AppHelper', 'View/Helper');

class CounterHelper extends AppHelper
{
    public $helpers = array("Html");

    /**
     * Cria a estrutura html dos contadores
     *
     * @param type $options
     * @return mixed
     */
    public function create($items = array())
    {
        $li = array();

        foreach ($items as $item) {
            $label = $item["label"];

            if (isset($item["conditions"])) {
                $item["conditions"] = base64_encode(serialize($item["conditions"]));
            } else {
                $item["conditions"] = base64_encode(serialize(array()));
            }

            if (!isset($item["url"])) {
                $item["url"] = array("controller" => Inflector::pluralize(lcfirst($item["model"])));
            }

            $span = $this->Html->tag("span", " ... ", array("class" => "label pull-right label-primary"));

            $options = array(
                "class" => "counter",
                "escape" => false,
                "data-model" => $item["model"],
                "data-conditions" => $item["conditions"]
            );

            if (!isset($item["data-alert-mode"])) {
                $options["data-alert-mode"] = "none";
            } else {
                $options["data-alert-mode"] = $item["data-alert-mode"];
            }

            $link = $this->Html->link($label . $span, $item["url"], $options);

            $li[] = $this->Html->tag("li", $link);
        }

        return $this->Html->tag("ul", implode("", $li), array("class" => "nav nav-pills nav-stacked"));
    }
}
