<?php
App::uses('AppHelper', 'View/Helper');

class VideoHelper extends AppHelper
{
    public $helpers = array("Html");
	public $defaults = array(
		'width' => 438,
		'height' => 246,
		'frameborder' => 0,
		'allowfullscreen' => 1,
		'src' => null
	);
	
	/**
	 * VideoHelper::embed
	 *
	 * Get iframe
	 *
	 * @param string $url
	 * @return string
	 */
	public function embed($url, $options = array())
	{
		$options = array_merge($this->defaults, $options);
		$options['src'] = $this->parseUrl($url);
		
		return $this->Html->tag('iframe', '', $options);
	}
	
	/**
	 * VideoHelper::_parseUrl
	 *
	 * Parse and clean URL
	 *
	 * @param string $url
	 * @return string
	 */
	public function parseUrl($url)
	{
		$video = str_replace('watch?v=', 'embed/', $url);
		$video = str_replace('youtu.be/', 'youtube.com/embed/', $video);

		if (preg_match('/(vimeo\.com)/', $video)) :
			$video = preg_replace('/^(https?\:\/\/vimeo\.com\/([0-9]+))$/i', '//player.vimeo.com/video/$2', $video);
		endif;
		
		$video = preg_replace('/\&index\=([a-zA-Z0-9\-\_]+)/', '', $video);
		$video = preg_replace('/\&list\=([a-zA-Z0-9\-\_]+)/', '', $video);
		$video = preg_replace('/\&feature\=([a-zA-Z0-9\-\_\.]+)/', '', $video);
		
		return $video;
	}
}