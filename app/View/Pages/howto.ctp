<?php $this->viewVars['requestCss'][] = array('bootstrap.min','animate.min'); ?>

<!--
#############################################
# Formulários                               # 
#############################################
-->

<h3>Criando um formulário</h3>

<p>

    <?php echo $this->Form->create(); ?>

    <?php echo $this->Form->input('name'); ?>
    <?php echo $this->Form->input('phone', array('label' => 'Telefone', 'class' => 'format-phone')); ?>
    <?php echo $this->Form->input('data', array('label' => 'Data', 'class' => 'format-date')); ?>
    <?php echo $this->Form->input('time', array('label' => 'Horario', 'class' => 'format-time')); ?>
    <?php echo $this->Form->input('cnpj', array('label' => 'CNPJ', 'class' => 'format-cnpj')); ?>
    <?php echo $this->Form->input('cpf', array('label' => 'CPF', 'class' => 'format-cpf')); ?>    
    <?php echo $this->Form->input('money', array('label' => 'Valor R$', 'class' => 'format-money')); ?>    
    <?php echo $this->Form->input('rate', array('label' => 'Taxa', 'class' => 'format-rate')); ?> 
    <?php echo $this->Form->input('lat', array('label' => 'Latitude', 'class' => 'format-geo-coord')); ?> 
    <?php echo $this->Form->input('integer', array('label' => 'Numero (1 - 99)', 'class' => 'format-integer', 'data-min-value' => '0', 'data-max-value' => '99')); ?>     

    <?php echo $this->Form->end(); ?>


</p>

<h4>Código:</h4>

<pre>

    &lt;?php echo $this->Form->create(); ?&gt;

    &lt;?php echo $this->Form->input('name'); ?&gt;
    &lt;?php echo $this->Form->input('phone', array('label' => 'Telefone', 'class' => 'format-phone')); ?&gt;
    &lt;?php echo $this->Form->input('data', array('label' => 'Data', 'class' => 'format-date')); ?&gt;
    &lt;?php echo $this->Form->input('time', array('label' => 'Horario', 'class' => 'format-time')); ?&gt;
    &lt;?php echo $this->Form->input('cnpj', array('label' => 'CNPJ', 'class' => 'format-cnpj')); ?&gt;
    &lt;?php echo $this->Form->input('cpf', array('label' => 'CPF', 'class' => 'format-cpf')); ?&gt;    
    &lt;?php echo $this->Form->input('money', array('label' => 'Valor R$', 'class' => 'format-money')); ?&gt;    
    &lt;?php echo $this->Form->input('rate', array('label' => 'Taxa', 'class' => 'format-rate')); ?&gt; 
    &lt;?php echo $this->Form->input('lat', array('label' => 'Latitude', 'class' => 'format-geo-coord')); ?&gt; 
    &lt;?php echo $this->Form->input('integer', array('label' => 'Numero (1 - 99)', 'class' => 'format-integer', 'data-min-value' => '0', 'data-max-value' => '99')); ?&gt;     

    &lt;?php echo $this->Form->end(); ?&gt;

</pre>

<div class="alert alert-info">
    <strong>Obs:</strong> Precisa dos javascripts (app.js, app.form.js, autoNumeric.js, jquery.maskedinput-1.3.js ,jquery-migrate-1.2.1.js)
</div>

<hr>

<!--
#############################################
# Imagens                                   # 
#############################################
-->

<h3>Criando imagens</h3>

<p>
    <?php echo $this->Html->image("chuck.jpg"); ?>
    <?php echo $this->Html->image($this->Image->resize("img/chuck.jpg", "200", "100")); ?>
    <?php echo $this->Html->image($this->Image->resize("img/chuck.jpg", "200", "100", true, true)); ?>
</p>

<h4>Código:</h4>

<pre>
    &lt;?php echo $this->Html->image("chuck.jpg"); ?&gt;
    // Aspect
    &lt;?php echo $this->Html->image($this->Image->resize("img/chuck.jpg","200","100")); ?&gt;
    // Crop
    &lt;?php echo $this->Html->image($this->Image->resize("img/chuck.jpg","200","100",true,true)); ?&gt;
</pre>

<div class="alert alert-info">
    O Helper <strong>Image</strong> não é uma lib do cake
</div>

<hr>

<!--
#############################################
# Links                                     # 
#############################################
-->

<h3>Criando links</h3>

<p>
    <?php echo $this->Html->link("Este link leva para a home", array("action" => "home")); ?>
    <br>
    <?php
    $img = $this->Html->image($this->Image->resize("img/chuck.jpg", "200", "100"));
    echo $this->Html->link($img, array("action" => "home"), array('escape' => false, 'alt' => 'Esta imagem leva para a home'));
    ?>
</p>

<h4>Código:</h4>

<pre>
    &lt;?php 
    echo $this->Html->link("Este link leva para a home",array("action"=>"home"));

    $img = $this->Html->image($this->Image->resize("img/chuck.jpg", "200", "100"));
    echo $this->Html->link($img, array("action" => "home"), array('escape' => false, 'alt' => 'Esta imagem leva para a home'));
    ?&gt;
</pre>

<div class="alert alert-info">
    <strong>Obs:</strong> Sempre utilize array na url para que a rota reversa do cake funcione
    <br>
    <?php
    $link = "http://book.cakephp.org/2.0/en/core-libraries/helpers/html.html";
    echo $this->Html->link($link, $link);
    ?>
</div>

<hr>

<!--
#############################################
# Translate                                 # 
#############################################
-->

<h3>Traduzindo</h3>

<p>
    <?php echo __("Este texto é traduzido automaticamente pelo cake"); ?>
</p>

<h4>Código:</h4>

<pre>
    &lt;php echo __("Este texto é traduzido automaticamente pelo cake"); ?&gt;
</pre>

<div class="alert alert-info">
    <?php
    $link = "http://book.cakephp.org/1.3/pt/The-Manual/Common-Tasks-With-CakePHP/Internationalization-Localization.html";
    echo $this->Html->link($link, $link);
    ?>
</div>

<hr>

<!--
#############################################
# Truncate                                  # 
#############################################
-->

<h3>Texto grande quebrou o layout?</h3>

<p>
    <?php echo $this->Text->truncate("Este texto é muito longo por isso precisa ser truncado", 20); ?>
</p>

<h4>Código:</h4>

<pre>
    &lt;php $this->Text->truncate("Este texto é muito longo por isso precisa ser truncado",20); ?&gt;
</pre>

<div class="alert alert-info">
    <?php
    $link = "http://book.cakephp.org/2.0/en/core-libraries/helpers/text.html";
    echo $this->Html->link($link, $link);
    ?>
</div>

