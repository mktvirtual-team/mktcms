<?php $this->viewVars['requestCss'][] = array('bootstrap.min','animate.min'); ?>

<div class="hero-unit">
    <h2><?php echo __('CAKEPHP - MKTCMS') ?></h2>
</div>

<div class="well">
    <h3>Features</h3>
    <p class="features">
        <?php echo $this->Html->image($this->Image->resize(WWW_ROOT . 'img/features/cake-logo.png', 70, 90, true, false), array('alt' => 'CakePHP')) ?>
        <?php echo $this->Html->image($this->Image->resize(WWW_ROOT . 'img/features/composer.png', 70, 90, true, false), array('alt' => 'Composer')) ?>
        <?php echo $this->Html->image($this->Image->resize(WWW_ROOT . 'img/features/jenkins.png', 70, 90, true, false), array('alt' => 'Jenkins')) ?>
        <?php echo $this->Html->image($this->Image->resize(WWW_ROOT . 'img/features/phpunit.png', 70, 90, true, false), array('alt' => 'PHPUnit')) ?>
        <?php echo $this->Html->image($this->Image->resize(WWW_ROOT . 'img/features/selenium.png', 70, 90, true, false), array('alt' => 'Selenium')) ?>
        <?php echo $this->Html->image($this->Image->resize(WWW_ROOT . 'img/features/phpspec.png', 70, 90, true, false), array('alt' => 'PHPSpec')) ?>
        <?php echo $this->Html->image($this->Image->resize(WWW_ROOT . 'img/features/gruntjs.png', 70, 90, true, false), array('alt' => 'GruntJS')) ?>
        <?php echo $this->Html->image($this->Image->resize(WWW_ROOT . 'img/features/jasmine.png', 120, 90, true, false), array('alt' => 'Jasmine')) ?>
        <?php echo $this->Html->image($this->Image->resize(WWW_ROOT . 'img/features/sass.png', 70, 90, true, false), array('alt' => 'SASS')) ?>
        <?php echo $this->Html->image($this->Image->resize(WWW_ROOT . 'img/features/compass.png', 70, 90, true, false), array('alt' => 'Compass')) ?>
        <?php echo $this->Html->image($this->Image->resize(WWW_ROOT . 'img/features/phantomjs.png', 70, 90, true, false), array('alt' => 'PhantomJS')) ?>
        <?php echo $this->Html->image($this->Image->resize(WWW_ROOT . 'img/features/qunit.png', 70, 90, true, false), array('alt' => 'QUnit')) ?>
    </p>
</div>

<div class="well">
    <h3>Objective</h3>
    <p>Simplify your life</p>
    <p><?php echo $this->Html->image($this->Image->resize(WWW_ROOT . 'img/features/vida-de-programador.png', 200, 100, true, false)) ?></p>
</div>

<div class="well">
    <h3>HOW?</h3>
    <p>Doing the magic with ( DRY / KISS / SRP / CRUD with new bake template / Migration / Test / Components and Behaviors)</p>
</div>

