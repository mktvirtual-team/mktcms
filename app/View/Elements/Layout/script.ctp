<?php echo $this->Html->scriptBlock('var URL_BASE = "' . Router::url("/", true) . '";', array('inline' => false)) ?>

<?php

$jsList = array(
    'vendor/jquery-1.11.0.min.js',
    'vendor/bootstrap.min.js',
    'vendor/jquery-migrate-1.2.1.js',
    'vendor/jquery.maskedinput-1.3.js',
    'vendor/autoNumeric.js',
    'app.form.js',
    'app.js',
    'default.js'
);

if (isset($this->viewVars['requestJs']))
    $jsList[] = $this->viewVars['requestJs'];

if (file_exists(JS . CURRENT_VIEW . '.js'))
    $jsList[] = CURRENT_VIEW . '.js';

echo $this->fetch('script');
echo $this->ScriptCombiner->js($jsList);
echo $this->element('sql_dump');

echo Analytics::renderTrackEventScript($this);
?>
