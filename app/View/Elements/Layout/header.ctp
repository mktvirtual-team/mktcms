<header>
    <div class="navbar">
        <div class="navbar-inner">
            <span class="brand"><?php echo $this->Html->image('features/cake-logo.png', array('width' => 27)) ?></span>
            <nav>
                <ul class="nav">
                    <li><?php echo $this->Html->link('home', array('action' => 'home'), array('title' => 'go home', 'class' => 'home')) ?></li>
                    <li><?php echo $this->Html->link('howto', array('action' => 'howto'), array('title' => 'frontenders start here')) ?></li>
                </ul>
            </nav>
        </div>
    </div>
</header>