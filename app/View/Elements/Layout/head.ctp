<?php
echo $this->Html->charset();
echo $this->Html->meta('icon');
echo $this->Html->meta(array('http-equiv' => 'X-UA-Compatible', 'content' => 'IE=edge,chrome=1'));
echo $this->Html->tag('title', $this->fetch('title'));
echo $this->Html->meta('description');
echo $this->Html->meta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1'));

echo $this->element('Layout/css');
?>