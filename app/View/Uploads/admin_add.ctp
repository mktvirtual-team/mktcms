<div class="content-grid">
	<?php echo $this->Form->create('Upload', array('type' => 'file')) ?>
	<?php # echo $this->Form->input('file'); // for upload with URI ?>
	<?php echo $this->Form->input('file.', array('type' => 'file', 'multiple' => 'multiple', 'after' => ' Tamanho máximo de 2mb')) ?>
	<?php echo $this->Form->input('extension', array('type' => 'hidden')) ?>
	<?php echo $this->Form->input('model', array('type' => 'hidden', 'value' => $model)) ?>
	<?php echo $this->Form->input('foreign_key', array('type' => 'hidden', 'value' => $foreignKey)) ?>
	<?php echo $this->Form->end('Enviar') ?>
</div>