(function(){
	'use strict';
	jQuery( function($) {
		$('.navbar').addClass('animated bounceInLeft');
		$('.hero-unit').addClass('animated bounceIn');
		$('.features img').addClass('animated bounceInUp');
	});
})();