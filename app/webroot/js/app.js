;(function($, window, App){

	jQuery(function($) {

		/**
		 * Do the magic!
		 * @returns {undefined}
		 */
		App.init = function() {
			App.initForm();

			return true;
		};
		App.init();
	});

})(jQuery, this, this.App);
