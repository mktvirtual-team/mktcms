/**
 * Init counters
 * @returns {undefined}
 */
App.initCounter = function() {

    $('.counter').each(function(){
        
        var data = {
            model: $(this).attr("data-model"),
            conditions: $(this).attr("data-conditions")
        };
        
        var _this = this;

        $.post(URL_BASE + "admin/home/count",data,function(response){
            
            if(response.count > 0 && $(_this).attr("data-alert-mode") === "when-not-empty")
            {
                $(_this).find("span").addClass("label-warning");
            }
            
            if(response.count === 0 && $(_this).attr("data-alert-mode") === "when-empty")
            {
                $(_this).find("span").addClass("label-warning");
            }
            
            $(_this).find("span").html(response.count);
            
        },'json');
        
    });
    
};
