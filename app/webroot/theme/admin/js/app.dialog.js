/**
 * Show a dialog with custom buttons
 * @param {string} title
 * @param {string|html} body
 * @param {array} buttons
 * @param {callback} callback
 * @returns boolean
 */
App.showDlg = function(title, content, buttons, callback,dont_hide_dlg) {

    var dlg = $("#dlg");

    dlg.find(".modal-title").html(title);
    dlg.find(".modal-body").empty().html(content);

    dlg.modal('show');

    var footer = dlg.find(".modal-footer").empty();
    footer.empty();

    $.each(buttons, function(idx, data) {
        footer.append('<button type="button" class="btn {2}" data-action="{0}">{1}</button>'.format(data.action, data.label, data.cls));
    });

    dlg.find(".btn").unbind("click").click(function() {
        
        if(!dont_hide_dlg)
        {
            dlg.modal('hide');
        }
        
        callback($(this).attr("data-action"));
    });

    return true;

};

/**
 * Show a dialog with delete and cancel buttons
 * @param {string} title
 * @param {string|html} content
 * @param {callback} callback
 * @returns {boolean}
 */
App.showDeleteCancelDlg = function(title, content, callback) {
    var buttons = [{action: "cancel", label: "Cancelar", cls: "btn-default"}, {action: "delete", label: "Excluir", cls: "btn-danger"}];
    App.showDlg("Atenção", content, buttons, function(result) {
        callback(result);
    });

    return true;
};

/**
 * Show a grid export dialog
 * @param {string} title
 * @param {string|html} content
 * @param {callback} callback
 * @returns {boolean}
 */
App.showExportGridDlg = function(title, callback) {

    var buttons = [{action: "cancel", label: "Cancelar", cls: "btn-default"}, {action: "export", label: "Exportar", cls: "btn-primary"}];

    var url = URL_BASE + "admin/" + CONTROLLER + "/export_fields";


    if (PLUGIN !== "")
    {
        url = URL_BASE + "admin/" + PLUGIN + "/" + CONTROLLER + "/export_fields";
    }

    App.showDlg("Exportar", $("<div></div>").load(url), buttons, function(result) {
        callback(result);
    });

    return true;
};

/**
 * Show a dialog with ok buttons
 * @param {string} title
 * @param {string|html} content
 * @param {callback} callback
 * @returns {boolean}
 */
App.showOkDlg = function(title, content, callback) {
    var buttons = [{action: "ok", label: "Ok", cls: "btn-default"}];
    App.showDlg("Atenção", content, buttons, function(result) {
        callback(result);
    });

    return true;
};

/**
 * Show a remote form dialog with ok save and cancel buttons
 * @param {string} title
 * @param {string|html} content
 * @param {callback} callback
 * @returns {boolean}
 */
App.showAjaxFormDlg = function(title, url, callback) {
    var buttons = [{action: "cancel", label: "Cancelar", cls: "btn-default"}, {action: "save", label: "Salvar", cls: "btn-success"}];

    var dlg = $("#dlg");
    
    App.showDlg(title, $("<div></div>").load(url, url, function() {

        var options = {
            dataType: "json",
            beforeSubmit: function(){
                
                dlg.find(".help-block").remove();
                dlg.find(".has-error").removeClass("has-error");
            
            },
            success: function(response){
                dlg.find("button").attr("disabled",false);
                if(response.result)
                {
                    dlg.modal('hide');
                    callback('saved');
                }
                else
                {
                    for(var field_name in response.errors)
                    {
                        var field = $(dlg).find("[name*='{0}']".format(field_name));
                        $(field).closest(".form-group").addClass("has-error");
                        $(field).after("<span class='help-block'>{0}</span>".format(response.errors[field_name]));
                    } 
                }
            }
        };

        $(this).find("form").ajaxForm(options);

    }), buttons, function(result) {
        if(result === "save")
        {
            dlg.find("button").attr("disabled",true);
            dlg.find("form").submit();
        }
        else
        {
            dlg.modal('hide');
            callback({result: 'cancel'});
        }
    },true);

    return true;
};
 
