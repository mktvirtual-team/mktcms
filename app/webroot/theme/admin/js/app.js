function App() {
}

/**
 * Do the magic!
 * @returns {undefined}      
 */
App.init = function() {

    $(".showtooltip").tooltip({html: true});
    
    $('.navbar-fixed-bottom').addClass("animated fadeInUp");

    this.initGrid();
    this.initCounter();    
    this.initForm();
    this.initAutocompleteBelongsTo();
    this.initAutocomplete();
    return true;

};


$(document).ready(function() {

    App.init();

});




