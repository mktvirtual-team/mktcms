/**
 * Init autocomplete fields
 * @returns {undefined}
 */
App.initAutocompleteBelongsTo = function() {

    $('.autocomplete-belongs-to').keydown(function(e) {
        if (e.keyCode === 9)
            return;

        var uniqid = $(this).attr("data-link");
        $("input[data-linked='" + uniqid + "']").val("");

    });

    $('.autocomplete-belongs-to').blur(function() {
        var uniqid = $(this).attr("data-link");
        if ($("input[data-linked='" + uniqid + "']").val() === "")
        {
            $(this).val("");
            $(this).parent().find(".icon-ok").remove();
        }

        if ($(this).val() === "")
            $("input[data-linked='" + uniqid + "']").val("").trigger('change');

    });

    $('.autocomplete-belongs-to').each(function() {

        var _this = this;

        
        var uniqid = $(_this).attr("data-link");
        var id = $("input[data-linked='" + uniqid + "']").val();
        var url = $(_this).attr("data-url");
        var field = $(_this).attr("data-field");
        var model = $(_this).attr("data-model");
        var conditions = $(_this).attr("data-conditions");

        var data = {
            model: model,
            field: field,
            id: id,
        };

        if (id > 0)
        {
            $.post(url + "/1", data, function(response) {
                if (response.result)
                {
                    $(_this).val(response.value);
                }
            });
        }

        //Bind Plugin            
        $(this).typeahead({
            ajax: {
                url: url,
                triggerLength: 1,
                preDispatch: function(value)
                {
                    $(_this).addClass("ui-autocomplete-loading");

                    var data = {
                        model: model,
                        field: field,
                        conditions: conditions,
                        value: value//conditions: conditions        
                    };

                    return data;

                },
                preProcess: function(data)
                {
                    $(_this).removeClass("ui-autocomplete-loading");
                    return data;
                }
            },
            itemSelected: function(item, val, text) {

                $("input[data-linked='" + uniqid + "']").val("" + val).trigger('change');
                $(_this).parent().find(".icon-ok").remove();
                $(_this).after("<span class='icon icon-ok' style='margin-top: -5px'></span>");
                $(_this).blur();
            }

        });

    });
};


App.initAutocomplete = function() {

    
    $('.autocomplete').each(function() {

        var _this = this;

        var url = $(_this).attr("data-url");
        var field = $(_this).attr("data-field");
        var model = $(_this).attr("data-model");
        var conditions = $(_this).attr("data-conditions");

        var data = {
            model: model,
            field: field,
            conditions: conditions
        };


        //Bind Plugin            
        $(this).typeahead({
            ajax: {
                url: url,
                triggerLength: 1,
                preDispatch: function(value)
                {
                    $(_this).addClass("ui-autocomplete-loading");

                    var data = {
                        model: model,
                        field: field,
                        conditions: conditions,
                        value: value//conditions: conditions        
                    };

                    return data;

                },
                preProcess: function(data)
                {
                    $(_this).removeClass("ui-autocomplete-loading");
                    return data;
                }
            }

        });

    });
};



