
App.initForm = function()
{
    this.initFormat();
    this.initSearchForm();
    this.datePicker();
    this.multiselect();
    this.initTextArea();
    this.initHider();
    this.initEditor();
};

/**
 * Init fields masks and formats
 * @returns {undefined}
 */
App.initFormat = function()
{

    $(".format-zipcode").mask("99999-999");
    $(".format-date").mask("99/99/9999");
    $(".format-time").mask("99:99:99");
    $(".format-cnpj").mask("99.999.999/9999-99");


    $(".format-cpf").mask("999.999.999-99");

    $(".format-phone").mask("(99) 9999-9999?9")
            .on('focusout', function(event) {
        var target, phone, element;
        target = (event.currentTarget) ? event.currentTarget : event.srcElement;
        phone = target.value.replace(/\D/g, '');
        element = $(target);
        element.unmask();
        if (phone.length > 10) {
            element.mask("(99) 99999-999?9");
        } else {
            element.mask("(99) 9999-9999?9");
        }
    });

    $('.format-money').autoNumeric('init', {aDec: ",", aSep: "."});
    $('.format-rate').autoNumeric('init', {aDec: ",", aSep: ".", mDec: 4});

    $(".format-geo-coord").autoNumeric('init', {mDec: 15, vMin: '-999.999999999999999'});

    $('.format-integer').each(function()
    {
        var min_value = $(this).attr("data-min-value");
        var max_value = $(this).attr("data-max-value");

        var options = {aDec: ",", aSep: ".", mDec: 0};

        if (min_value)
        {
            options["vMin"] = min_value;
        }

        if (min_value)
        {
            options["vMax"] = max_value;
        }

        $(this).autoNumeric('init', options);
    });

};

/**
 * Bind events to search form
 * @returns {undefined}
 */

App.initSearchForm = function()
{

    $(".btn-clear-form").click(function() {

        var frm = $(this).closest("form");
        $(frm).find(":not(input[type='radio'],input[type='checkbox'],input[type='submit'])").val("");
        $(frm).find("input[type='radio'],input[type='checkbox']").attr("checked", false);
        $(frm).find("select option[value='']").attr('selected', true);
        $(frm).find("input[type='submit']").click();
    });

};

/*
 * Aplica Jquery UI datepicker em todos os campos com a class .datepicker
 */
App.datePicker = function()
{

    $(".format-datepicker").datepicker();

    $(".format-datepicker").change(function() {
        if ($(this).val() !== "" && $(this).attr("data-min-date-of") !== "")
        {
            $($(this).attr("data-min-date-of")).datepicker('option', 'minDate', $(this).val());
        }
    });

    $(".format-datepicker").change();

};

/**
 * Transforma um campo select[multiselect] em multiselect
 * @returns {void}
 */
App.multiselect = function()
{
    $('select[multiple]').multiselect({
        numberDisplayed: 1,
        buttonWidth: 216,
        nonSelectedText: "Selecione",
        nSelectedText: "selecionados"
    });
};

/**
 * Aplica tamanho maximo em campos textarea
 * @returns {void}
 */
App.initTextArea = function() {

    $("textarea[maxlength]").each(function() {
        var label = $(this).closest("div").find("label");
        $(label).attr("content", "{0} - {1} de {2}".format($(label).html()));

    });

    $("textarea[maxlength]").unbind("keyup").keydown(function(event) {

        var key = event.which;

        var maxLength = $(this).attr("maxlength");
        var length = this.value.length;

        //todas as teclas incluindo enter
        if (key >= 33 || key === 13) {

            if (length >= maxLength) {

                event.preventDefault();
            }

        }
        var label = $(this).closest("div").find("label");

        $(label).html($(label).attr("content").format("", length, maxLength));
    });

    $("textarea[maxlength]").keydown();

};

/**
 * Configura datepicker para português
 */
$(document).ready(function() {
    $.datepicker.regional['pt'] = {
        closeText: 'Fechar',
        prevText: '<Anterior',
        nextText: 'Seguinte',
        currentText: 'Hoje',
        monthNames: ['Janeiro', 'Fevereiro', 'Mar&ccedil;o', 'Abril', 'Maio', 'Junho',
            'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun',
            'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
        dayNames: ['Domingo', 'Segunda-feira', 'Ter&ccedil;a-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'S&aacute;bado'],
        dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'S&aacute;b'],
        dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'S&aacute;b'],
        weekHeader: 'Sem',
        dateFormat: 'dd/mm/yy',
        firstDay: 0,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['pt']);

});


/**
 * Esconde o form-group automaticamente baseando-se no valor de outro campo
 * ... class="hide-when" data-field="#FooType" data-equals="" ...
 *
 * Exemplo:
 *
 * @returns {void}
 */
App.initHider = function()
{
    $('.hide-when').each(function() {

        var _this = this;
        var field = $(this).attr("data-field");
        var equals = $(this).attr("data-equals");

        $(field).change(function() {

            if ($(this).val() == equals)
            {
                $(_this).closest(".form-group").hide().val("").attr("disabled", true);
            }
            else
            {
                $(_this).closest(".form-group").show().attr("disabled", false);
            }
        });

        $(field).change();

    });
};

/**
 * Transforma um textarea em um editor html(CKEDITOR)
 * @returns {void}
 */
App.initCKEditor = function()
{
    if (typeof CKEDITOR === 'undefined') {
        return;
    }

    var config = {};
    config.language = "pt-br";
    config.toolbar = [
        {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic']},
        {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList']}
    ];

    $('.editor').ckeditor(config);

};
/**
 * Transforma um textarea em um editor com wyshtml5
 * @return {void}
 */
App.initEditor = function()
{
    $('.editor').wysihtml5({
        toolbar : {
            "font-styles": false, //Font styling, e.g. h1, h2, etc. Default true
            "emphasis": true, //Italics, bold, etc. Default true
            "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
            "html": true, //Button which allows you to edit the generated HTML. Default false
            "link": true, //Button to insert a link. Default true
            "image": true, //Button to insert an image. Default true,
            "color": false, //Button to change color of font
            "blockquote": false, //Blockquote
        }
    });
};
