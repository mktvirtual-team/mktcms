<?php

App::uses('Shell', 'Console');
App::uses('AppShell', 'Console/Command');
App::uses('String', 'Utility');
App::uses('ClassRegistry', 'Utility');

App::uses('AppShell', 'Console/Command');
App::uses('QueueShell', 'Queue.Console/Command');

App::uses('QueueAppModel', 'Queue.Model');
App::uses('QueuedTask', 'Queue.Model');

class WebQueue extends QueueShell
{

    public $uses = array(
        'Queue.QueuedTask'
    );
    public $Tasks;
    public $name = null;
    public $plugin = null;
    public $tasks = array();
    public $taskNames = array();

    public function __construct()
    {
        $this->QueuedTask = ClassRegistry::init('Queue.QueuedTask');
        if (!$this->name) {
            $this->name = Inflector::camelize(str_replace(array('Shell', 'Task'), '', get_class($this)));
        }
        $this->Tasks = new TaskCollection($this);
        $parent = get_parent_class($this);
        if ($this->tasks !== null && $this->tasks !== false) {
            $this->_mergeVars(array('tasks'), $parent, true);
        }
    }

    public function out($message = null, $newlines = 1, $level = Shell::NORMAL)
    {
        /*
          echo '<pre>';
          echo $message;
          echo '</pre>';
         */
    }

}