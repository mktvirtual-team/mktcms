<?php
$config['Queue']['sleeptime'] = 10;
$config['Queue']['gcprop'] = 10;
$config['Queue']['defaultworkertimeout'] = 120;
$config['Queue']['defaultworkerretries'] = 2;
$config['Queue']['workermaxruntime'] = 3600; //300
$config['Queue']['exitwhennothingtodo'] = true; //true