<?php
class ProSeed extends SeedShell
{

    public $uses = array('Operator');

    function up()
    {

        $this->_seedOperators();
        self::consoleWriteLine("############## Seed Completed XD ################");
        
    }
    
    function down()
    {
         self::_consoleWriteLine("Deleting...");

        $db = ConnectionManager::getDataSource('default');
        $db->rawQuery("SET foreign_key_checks = 0");

        $db->rawQuery("truncate operators;");
        $db->rawQuery("truncate error_logs;");

        $db->rawQuery("truncate uploads;");
        $uploadFolder = new Folder(ROOT . '/uploads');
        if ($uploadFolder->delete()) {
            self::_consoleWriteLine("uploads excluídos [OK]");
        }

        $db->rawQuery("SET foreign_key_checks = 1");

        self::_consoleWriteLine("[OK]");
    }

    function _seedOperators()
    {
        self::_consoleWriteLine("seedOperators...");

        // Admin
        $data = array(
            'name' => 'Admin',
            'email' => 'admin@mktvirtual.com.br',
            'password' => '123abc',
            'cpassword' => '123abc',
            'type' => Operator::TYPE_ADMINISTRATOR
        );

        $this->Operator->create();
        if (!$this->Operator->save($data))
            var_dump($this->Operator->validationErrors);

        // Operator
        $data = array(
            'name' => 'Operador',
            'email' => 'operator@mktvirtual.com.br',
            'password' => '123abc',
            'cpassword' => '123abc',
            'type' => Operator::TYPE_OPERATOR
        );

        $this->Operator->create();
        if (!$this->Operator->save($data))
            var_dump($this->Operator->validationErrors);

        self::_consoleWriteLine("[OK]");
    }

    static function _consoleWriteLine($value)
    {
        echo ($value . "\n");
    }

    static function _generateText()
    {
        return "Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralh";
    }

}