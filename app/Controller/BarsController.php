<?php
App::uses('AppController', 'Controller');

/**
 * @menu(Listar = "bars/asdasd", Adicionar = "Adicionar")
 */
class BarsController extends AppController
{
    public $paginate = array(
        'limit' => 15,
    );

    public function beforeFilter()
    {
        parent::beforeFilter();

        ClassRegistry::init($this->model);

        $this->grid = array(
            array("field" => "Bar.name"),
        );
    }

    public function admin_index()
    {
        $options = array(
            "name" => array("operation" => "contain"),
        );

        $this->paginate['conditions'] = SearchComponent::createConditions($this->request, $options);
        $this->paginate['order'] = SearchComponent::createOrder($this->request, array('Bar.name' => 'asc'));

        $this->set('grid', $this->grid);

        if (!isset($this->params->named["export"])) {
            $this->set('result', $this->paginate($this->model));
        } else {
            $this->set('result', ClassRegistry::init($this->model)->find('all', $this->paginate));
            $this->render("/Common/admin_export");
        }
    }
}
