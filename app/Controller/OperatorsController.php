<?php

App::uses('AppController', 'Controller');

class OperatorsController extends AppController
{

    public $components = array('GoogleAuth');
    public $paginate = array(
        'limit' => 15,
    );

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow('admin_google', 'admin_remember', 'admin_new_password');

        ClassRegistry::init($this->model);

        $this->grid = array(
            array("field" => "Operator.name"),
            array("field" => "Operator.email")
        );
    }

    public function admin_index()
    {
        $options = array(
            "name" => array("operation" => "contain"),
            "email" => array("operation" => "contain")
        );

        $this->paginate['conditions'] = SearchComponent::createConditions($this->request, $options);
        $this->paginate['order'] = SearchComponent::createOrder($this->request, array('Operator.id' => 'asc'));

        if ($this->Auth->user("type") == Operator::TYPE_OPERATOR) {
            $this->paginate['conditions'][] = array("Operator.type <>" => Operator::TYPE_ADMINISTRATOR);
        }

        $this->set('grid', $this->grid);

        if (!isset($this->params->named["export"])) {
            $this->set('result', $this->paginate($this->model));
        } else {
            $this->set('result', ClassRegistry::init($this->model)->find('all', $this->paginate));
            $this->render("/Common/admin_export");
        }
    }

    public function admin_edit($id = null)
    {
        if (AuthComponent::user("type") == Operator::TYPE_OPERATOR && AuthComponent::user("id") != $id) {
            $this->showError(array("controller" => "home"));
        }

        $model = $this->model;

        $this->$model->id = $id;

        if (!$this->$model->exists()) {
            $this->showError(array("controller" => "home"));
        }

        if ($this->request->is('get')) {
            $this->request->data = $this->$model->read();
            unset($this->request->data['Operator']['password']);
        } else {

            if ($this->Auth->user("type") == Operator::TYPE_OPERATOR && $this->request->data["Operator"]["id"] != $id) {
                $this->showError(array("controller" => "home"));
            }

            if ($this->$model->save($this->request->data)) {
                if ($this->Auth->user("type") == Operator::TYPE_ADMINISTRATOR) {
                    $this->showSuccess(array("action" => "index"));
                } else {
                    $this->showSuccess(array("controller" => "home", "action" => "index"));
                }
            } else {
                $this->showError();
            }
        }
    }

    public function admin_login()
    {
        $this->layout = "login";

        if ($this->Auth->user()) {
            return $this->redirect($this->Auth->redirect());
        }

        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                return $this->redirect($this->Auth->redirect());
            } else {
                $this->showError(null, "Usuário e(ou) senha inválido");
            }
        }
    }

    public function admin_logout()
    {
        $this->redirect($this->Auth->logout());
    }

    /**
     * Action que usa o componente GoogleAuth para criar e
     * logar o opperador com o e-mail ...@mktvirtual.com.br
     *
     * @return void
     */
    public function admin_google()
    {
        $this->autoRender = false;
        try {
            $url = $this->GoogleAuth->login();
            $this->redirect($url);
        } catch (Exception $e) {
            return $this->redirect($this->GoogleAuth->fail());
        }
    }

    public function admin_remember()
    {
        $this->layout = 'login';

        if ($this->request->is('get') || !isset($this->request->data['Operator']['email'])) {
            return false;
        }

        $this->Operator->validate = array(
            'email' => array(
                'rule' => array('email'),
                'message' => __('E-mail inválido')
            )
        );

        $this->Operator->set($this->data);
        if (!$this->Operator->validates()) {
            return false;
        }

        $operator = $this->Operator->findByEmail($this->request->data['Operator']['email']);
        if (!$operator) {
            return $this->showError(null, 'E-mail não encontrado!');
        }

        $this->Operator->id = $operator['Operator']['id'];
        if (!empty($operator['Operator']['hash'])) {
            $this->Operator->sendResetPassword($this->Operator->id);
        } else {
            if ($this->Operator->saveField('hash', md5(uniqid(microtime() + $operator['Operator']['id'])), false)) {
                $this->Operator->sendResetPassword($this->Operator->id);
            }
        }
        $this->showSuccess(array('controller' => 'operators', 'action' => 'login', 'admin' => true), 'Enviamos um e-mail com o link para criar uma nova senha!');
    }

    public function admin_new_password($hash = null)
    {
        $this->layout = 'login';

        if (!empty($hash) && $this->request->is('get')) {
            $operator = $this->Operator->findByHash($hash);
            if (!$operator) {
                return $this->redirect(array('controller' => 'operators', 'action' => 'login', 'admin' => true));
            }
            $this->Session->write('reset', $operator['Operator']);
        }

        if ($this->Session->check('reset') && !empty($this->data)) {
            $operator = $this->Session->read('reset');
            $this->Operator->validate = array('password' => $this->Operator->validate['password']);
            $this->Operator->id = $operator['id'];
            $this->Operator->data = null;
            $this->Operator->set($this->data);

            if (!$this->Operator->validates()) {
                return false;
            }

            if ($this->Operator->save($this->data)) {
                $this->Operator->saveField('hash', null, false);
                $this->Session->delete('reset');
                $this->showSuccess(array('controller' => 'operators', 'action' => 'login', 'admin' => true), 'Senha redefinida com sucesso!');
            }
        }
    }
}
