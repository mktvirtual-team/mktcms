<?php

App::uses('Component', 'Controller');
App::import('Vendor', 'openid');

/**
 * Componente para autenticação com o google
 */
class GoogleAuthComponent extends Component
{
	public $openid = null;
	public $components = array('Auth');

	/**
	 * Metodo responsável pela autenticação com o Google usando OpenID/OAuth
	 * Na primeira chamada, é retornado o link para autorização
	 * Na seguinte, já retorna o objeto com o nome e e-mail do usuário
	 *
	 * @return array - URL para redirecionar o usuário
	 * @throws ErrorException - Na falha de alguma chamada do LightOpenID
	 */
	public function login()
	{
		$openid = new LightOpenID;

		if (!$openid->mode):
			$openid->identity = 'https://www.google.com/accounts/o8/id';
			$openid->required = array('namePerson/first', 'namePerson/last', 'contact/email');
			return $openid->authUrl();
		endif;

		if ($openid->validate()):
			$attributes = $openid->getAttributes();
			$email = $attributes['contact/email'];

			if (empty($email) || $email == '' || !preg_match('/(\@mktvirtual\.com\.br)/i', $email)) {
				return $this->fail();
			}

			$Operator = ClassRegistry::init('Operator');
			$findOperator = $Operator->findByEmail($email);

			if (!$findOperator) {
				return $this->_createOperator($Operator, $attributes);
			}

			$this->Auth->login($findOperator['Operator']);
			return array('admin' => true, 'controller' => 'home', 'action' => 'index');
		endif;

		return $this->fail();
	}

	/**
	 * Cria o Operador usando as informações do Google
	 *
	 * @return array
	 */
	public function _createOperator(Model $Operator, $attributes = array())
	{
		$operatorData = array(
			'Operator' => array(
				'name' => $attributes['namePerson/first'] . ' ' . $attributes['namePerson/last'],
				'email' => $attributes['contact/email'],
				'password' => substr(md5(uniqid(rand(), true)), 0, 8),
				'type' => 2,
				'active' => 1
			)
		);

		$Operator->create();

		if ($Operator->save($operatorData, false)):
			$operatorData['Operator']['id'] = $Operator->id;
			$this->Auth->login($operatorData['Operator']);
			return array('admin' => true, 'controller' => 'home', 'action' => 'index');
		endif;

		return $this->fail();
	}

	/**
	 * Redireciona para a tela de login caso ocorra alguma falha
	 *
	 * @return array
	 */
	public function fail()
	{
		return array('admin' => true, 'controller' => 'operators', 'action' => 'login');
	}
}
