<?php

class SearchComponent extends Component
{
    public static function createConditions($request, $options = array())
    {
        $conditions = array();

        $fields = Set::flatten($request->query);

        $request->data = $request->query;

        foreach ($fields as $field => $value) {
            if (isset($options[$field]["ignore"]) && ($options[$field]["ignore"] == true)) {
                continue;
            }

			if (in_array($field, array("sidx", "sord")) || !$value) {
				continue;
			}

            if ($field == "page") {
                continue;
            }

            $operation = isset($options[$field]["operation"]) ? $options[$field]["operation"] : "";

			if (isset($options[$field]["field"])) {
				$field = $options[$field]["field"];
			}

            $conditions[] = self::_generateConditions($operation, $field, $value);
        }

        return $conditions;
    }

    public static function _generateConditions($operation, $field, $value)
    {
        $conditions = array();

        switch ($operation) {
            case "<":
                $conditions[] = array($field.$operation => $value);
                break;
            case ">":
                $conditions[] = array($field.$operation => $value);
                break;
             case "contain":
                $conditions[] = array("$field LIKE " => "%$value%");
                break;
            default:
                $conditions[] = array($field => $value);
                break;
        }

        return $conditions;
    }

    public static function createOrder($request, $default, $sidx_alias = array())
    {
        if (isset($request->query["sidx"]) && isset($request->query["sord"])) {
            if (isset($sidx_alias[$request->query["sidx"]])) {
                $sidx = $sidx_alias[$request->query["sidx"]];
            } else {
                $sidx = $request->query["sidx"];
            }

            return array($sidx => $request->query["sord"]);
        }

        return $default;
    }
}
