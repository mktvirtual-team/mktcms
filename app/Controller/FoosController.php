<?php
App::uses('AppController', 'Controller');

class FoosController extends AppController
{
    public $paginate = array(
        'limit' => 15,
    );

    public $grid = array();

    public function beforeFilter()
    {
        parent::beforeFilter();

        ClassRegistry::init($this->model);

        $this->grid = array(
            array("field" => "Foo.name"),
            array("field" => "Foo.type", "format" => function($value) {
				return Foo::$types[$value];
			}),
            array("field" => "Bar.name"),
            array("field" => "Foo.phone", "hidden" => true),
            array("field" => "Foo.zipcode"),
            array("field" => "Foo.upload", 'format' => array(
                '_formatImage' => array(
                    'width' => 80,
                    'height' => 80,
                    'aspect' => true,
                    'crop' => false
                )
            ))
        );

		$this->Auth->allow('index');
    }

    public function admin_add($action = 'index')
    {
        parent::admin_add();
        $this->set("bars", ClassRegistry::init("Bars")->find("list"));
    }

    public function admin_edit($id = null)
    {
        parent::admin_edit($id);
        $this->set("bars", ClassRegistry::init("Bars")->find("list"));
    }

    public function admin_index()
    {
        $options = array(
            "name" => array("operation" => "contain", "field" => "Foo.name"),
            "bar_name" => array("operation" => "contain", "field" => "Bar.name")
        );

        $this->paginate['contain'] = array("Bar" => array("fields" => "Bar.name"));

        $this->paginate['conditions'] = SearchComponent::createConditions($this->request, $options);
        $this->paginate['order'] = SearchComponent::createOrder($this->request, array('Foo.id' => 'desc'));

        $this->set('grid', $this->grid);

        if (!isset($this->params->named["export"])) {
            $this->set('result', $this->paginate($this->model));
        } else {
            $this->set('result', ClassRegistry::init($this->model)->find('all', $this->paginate));
            $this->render("/Common/admin_export");
        }

        $this->set("bars", ClassRegistry::init("Bars")->find("list"));
    }
	
	/**
	 * List foos
	 *
	 * @return mixed
	 */
	public function index()
	{
		$foos = $this->Foo->find('all');
        $this->set(array(
            'foos' => $foos,
            '_serialize' => array('foos')
        ));
	}
}