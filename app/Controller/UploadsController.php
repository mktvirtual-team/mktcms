<?php

App::uses('AppController', 'Controller');

class UploadsController extends AppController
{

    /**
     * Save files
     *
     * @param array $this->request->data
     * @return mixed
     */
    public function admin_add($action = 'index')
    {
        $model = $this->params['named']['model'];
        $foreignKey = $this->params['named']['foreign_key'];

        $this->set(array(
            'model' => $model,
            'foreignKey' => $foreignKey
        ));

        if ($this->request->is('post') && !empty($this->request->data)) {
            $saveFiles = $this->Upload->multipleUpload($this->request->data);
            # $saveFiles = $this->Upload->save($this->request->data); for upload with URI
            $redirect = array(
                'action' => 'add',
                'model' => $model,
                'foreign_key' => $foreignKey
            );
            $this->showMessage($redirect, 'success_message', json_encode($saveFiles));
        }
    }

    /**
     * Download Files
     *
     * @param int $id
     * @return resource
     */
    public function admin_download($id)
    {
        $this->view = 'Media';
        $this->viewClass = 'Media';

        $this->Upload->id = $id;

        if (!$this->Upload->exists()) {
            throw new NotFoundException('Arquivo não encontrado!');
        }

        $upload = $this->Upload->read();

        $path = ROOT . $upload['Upload']['path'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);

        $params = array(
            'id' => $upload['Upload']['file'],
            'file' => str_replace('.' . $ext, '', $upload['Upload']['file']),
            'download' => true,
            'extension' => $ext,
            'path' => str_replace($upload['Upload']['file'], '', $path)
        );
        $this->set($params);
    }

    /**
     * Show File
     *
     * @param int $id
     * @param string $type
     * @return resource
     */
    public function admin_show($id, $type = 'path')
    {
        $this->autoRender = false;
        $this->Upload->id = $id;

        $upload = $this->Upload->read();
        $path = $upload['Upload'][$type];
        $size = getimagesize($path);
        $handle = fopen($path, "rb");

        header("Content-type: {$size['mime']}");
        header("Content-Length: " . filesize($path));
        fpassthru($handle);
        fclose($handle);
    }
}
