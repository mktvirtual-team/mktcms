<?php
App::uses('AppController', 'Controller');

class LogsController extends AppController
{
    public $paginate = array(
        'limit' => 10,
    );
    public $grid = array();

    public function beforeFilter()
    {
        parent::beforeFilter();

        ClassRegistry::init($this->model);

        $this->grid = array(
            array("field" => "Log.who"),
            array("field" => "Log.action", "format" => function($value) {
                    return Log::$actions[$value];
            }),
            array("field" => "Log.model", "format" => function($model, $item) {
                $full_name = $model;

                if (!empty($item["Log"]["plugin"])) {
                    $full_name = $item["Log"]["plugin"] . "." . $model;
                }

                ClassRegistry::init($full_name);

                return $model::$label . " - [ " . $item["Log"]["model_id"] . " ]";
            }),
            array("field" => "Log.data", "options" => array("width" => "400"), "format" => function($value, $item) {
                return Log::changes($item["Log"]["model"], $value);
            }),
            array("field" => "Log.created")
        );
    }

    public function admin_index()
    {
        $options = array(
            "name" => array("operation" => "contain"),
        );

        $this->paginate['conditions'] = SearchComponent::createConditions($this->request, $options);
        $this->paginate['order'] = SearchComponent::createOrder($this->request, array('Log.id' => 'desc'));

        $this->set('grid', $this->grid);

        if (!isset($this->params->named["export"])) {
            $this->set('result', $this->paginate($this->model));
        } else {
            unset($this->paginate["limit"]);
            $this->set('result', ClassRegistry::init($this->model)->find('all', $this->paginate));
            $this->render("/Common/admin_export");
        }
    }
}
