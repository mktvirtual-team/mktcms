<?php
App::uses('AppController', 'Controller');

class ConfigsController extends AppController
{
    public $paginate = array(
        'limit' => 2,
    );

    public function beforeFilter()
    {
        parent::beforeFilter();

        ClassRegistry::init($this->model);

        $this->grid = array(
            array("field" => "Config.label", "label" => "Parametro"),
            array("field" => "Config.value")
        );

        ClassRegistry::init("Operator");
    }

    public function admin_index()
    {
        $options = array(
            "name" => array("operation" => "contain"),
        );

        if (AuthComponent::user("type") != Operator::TYPE_ADMINISTRATOR) {
            $this->request->query["editable"] = true;
        }

        $this->paginate['conditions'] = SearchComponent::createConditions($this->request, $options);
        $this->paginate['order'] = SearchComponent::createOrder($this->request, array('Config.name' => 'asc'));

        $this->set('grid', $this->grid);

        if (!isset($this->params->named["export"])) {
            $this->set('result', $this->paginate($this->model));
        } else {
            $this->set('result', ClassRegistry::init($this->model)->find('all', $this->paginate));
            $this->render("/Common/admin_export");
        }
    }
}
