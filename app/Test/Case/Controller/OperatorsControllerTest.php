<?php

App::uses('Operatorsontroller', 'Controller');

class OperatorsControllerTest extends ControllerTestCase {

	public $fixtures = array(
		'app.operator',
		'app.queued_task'
	);
	
	public function setUp() {
		parent::setUp();
		$this->Operator = ClassRegistry::init('Operator');
		$this->Operators = $this->generate('Operators', array(
			'models' => array(),
		    'components' => array(
				'Session',
				'RequestHandler',
				'Auth'
			)
		));
	}
	
	public function testInstance() {
		$this->assertIsA($this->Operators, 'OperatorsController');
		$this->assertIsA($this->Operators->Operator, 'Operator');
	}
	
	public function testAdminIndex() 
	{
		$this->Operators->Auth->staticExpects($this->any())->method('user')->with('type')->will($this->returnValue(1));
		$returnVars = $this->testAction('/admin/operators/index', array('return' => 'vars', 'method' => 'get'));
		$this->assertArrayHasKey('result', $returnVars);
		$this->assertTrue(count($returnVars['result']) == 1);
	}
	
	public function testAdminEdit() {
		$data = array(
			'Operator' => array(
				'id' => 1,
				'name' => 'plastic',
				'email' => 'plastic@mktvirtual.com.br'
			)
		);
		$this->Operators->Auth->staticExpects($this->any())->method('user')->with('type')->will($this->returnValue(1));
		$result = $this->testAction('/admin/operators/edit/1', array('data' => $data, 'method' => 'put'));
		$operator = $this->Operators->Operator->findById(1);
		$this->assertTrue($operator['Operator']['name'] == 'plastic');
	}
	
	public function testAdminEditWithMethodGet() {
		$data = array(
			'Operator' => array(
				'id' => 1,
				'name' => 'plastic',
				'email' => 'plastic@mktvirtual.com.br'
			)
		);
		$this->Operators->Auth->staticExpects($this->any())->method('user')->with('type')->will($this->returnValue(1));
		$result = $this->testAction('/admin/operators/edit/1', array('data' => $data, 'method' => 'get'));
		$operator = $this->Operators->Operator->findById(1);
		$this->assertTrue($operator['Operator']['name'] != 'plastic');
	}

	public function testAdminLogin()
	{
		$data = array(
			'Operator' => array(
				'email' => 'admin@admin.com',
				'password' => '123456'
			)
		);
		$this->testAction('/admin/operators/login', array('method' => 'post', 'data' => $data));
	}
	
	public function testAdminLoginSuccess()
	{
		$this->Operators->Auth->expects($this->any())->method('login')->will($this->returnValue(true));
		$this->testAction('/admin/operators/login', array('method' => 'post'));
	}
	
	public function testAdminLoginLogged()
	{
		$this->Operators->Auth->staticExpects($this->any())->method('user')->will($this->returnValue(true));
		$this->testAction('/admin/operators/login', array('return' => 'vars', 'method' => 'get'));
	}
	
	public function testAdminLogout()
	{
		$this->Operators->Auth->staticExpects($this->any())->method('user')->with('type')->will($this->returnValue(1));
		$this->testAction('/admin/operators/logout', array('return' => 'vars', 'method' => 'get'));
	}
	
	public function testAdminGoogle()
	{
		$this->testAction('/admin/operators/google', array('return' => 'vars', 'method' => 'get'));
	}
	
	public function testAdminRememberFail()
	{
		$this->testAction('/admin/operators/remember', array('method' => 'get'));
	}
	
	public function testAdminRememberPost()
	{
		$this->testAction('/admin/operators/remember', array('method' => 'post'));
	}
	
	public function testAdminRememberPostFail()
	{
		$data = array(
			'Operator' => array(
				'email' => 'admin@admin'
			)
		);
		$this->testAction('/admin/operators/remember', array('method' => 'post', 'data' => $data));
	}
	
	public function testAdminRememberPostEmailNotExist()
	{
		$data = array(
			'Operator' => array(
				'email' => 'admin@admin.com.br'
			)
		);
		$this->testAction('/admin/operators/remember', array('method' => 'post', 'data' => $data));
	}
	
	public function testAdminRememberPostSuccess()
	{
		$data = array(
			'Operator' => array(
				'email' => 'admin@admin.com'
			)
		);
		$this->testAction('/admin/operators/remember', array('method' => 'post', 'data' => $data));
	}
	
	public function testAdminRememberPostSuccessWithHash()
	{
		$this->Operators->Operator->id = 1;
		$this->Operators->Operator->save(array('id' => 1, 'hash' => ''), false);
		
		$data = array(
			'Operator' => array(
				'email' => 'admin@admin.com'
			)
		);
		$this->testAction('/admin/operators/remember', array('method' => 'post', 'data' => $data));
	}
	
	public function testAdminNewPassword()
	{
		$this->testAction('/admin/operators/new_password', array('method' => 'get'));
	}
	
	public function testAdminNewPasswordWithInvalidHash()
	{
		$this->testAction('/admin/operators/new_password/7367846327862578', array('method' => 'get'));
	}
	
	public function testAdminNewPasswordWithValidHash()
	{
		$this->testAction('/admin/operators/new_password/hash', array('method' => 'get'));
	}
	
	public function testAdminNewPasswordWithValidHashWithPostAndInvalidData()
	{
		$data = array(
			'Operator' => array(
				'password' => '123456',
				'cpassword' => '12345'
			)
		);
		$this->Operators->Session->expects($this->any())->method('check')->with('reset')->will($this->returnValue(true));
		$this->testAction('/admin/operators/new_password/', array('method' => 'post', 'data' => $data));
	}
	
	public function testAdminNewPasswordWithValidHashWithPostAndValidData()
	{
		$data = array(
			'Operator' => array(
				'password' => '123456',
				'cpassword' => '123456'
			)
		);
		$this->Operators->Session->expects($this->any())->method('check')->with('reset')->will($this->returnValue(true));
		$this->testAction('/admin/operators/new_password/', array('method' => 'post', 'data' => $data));
	}
}