<?php 
App::uses('CakeTestSuite', 'TestSuite');

class AllModelTest extends CakeTestSuite
{
	public static function suite()
	{
        # Criando a nova suite de testes (ou grupo de testes)
        $suite = new CakeTestSuite('All Model Tests');

        # Adicionando diretório onde os testes se encontram
        $suite->addTestDirectory(TESTS . 'Case' . DS . 'Model');

        return $suite;
    }
}