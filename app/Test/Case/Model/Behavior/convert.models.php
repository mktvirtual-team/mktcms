<?php
if (!defined('CAKEPHP_UNIT_TEST_EXECUTION')) {
	define('CAKEPHP_UNIT_TEST_EXECUTION', 1);
}

class Convert extends CakeTestModel {
	var $actsAs = array(
		'Convertable'
	);
}