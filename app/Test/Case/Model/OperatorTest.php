<?php
App::uses('Operator', 'Model');

/**
 * Operator Test Case
 *
 */
class OperatorTest extends CakeTestCase
{

	public $fixtures = array(
		'app.operator',
		'app.queued_task'
	);

	public function setUp()
	{
		parent::setUp();
		$this->Operator = ClassRegistry::init('Operator');
	}
	
	public function testConstsVariables()
	{
		$this->assertTrue(Operator::TYPE_OPERATOR === 1);
		$this->assertEquals(Operator::$types[Operator::TYPE_OPERATOR], 'Operador');
		$this->assertEquals(Operator::$labels['email'], 'E-mail');
		$this->assertEquals(Operator::$label, 'Operador');
	}
	
	public function testInstance()
	{
		$this->assertIsA($this->Operator, 'Operator');
	}
	
	public function testAdd()
	{
		$data = array(
			'type' => 1,
			'email' => 'andre@mktvirtual.com.br',
			'password' => '12345',
			'name' => 'plastic',
			'active' => 1
		);
		
		$this->Operator->create();
		$this->Operator->set($data);
		$savedWithWrongPassword = $this->Operator->save($data, true);
		
		$this->assertFalse($savedWithWrongPassword);
		$this->assertArrayHasKey('password', $this->Operator->validationErrors);
		
		$data['password'] = '123456';
		$savedWithoutConfirmPassword = $this->Operator->save($data, true);
		$this->assertFalse($savedWithoutConfirmPassword);
		$this->assertArrayHasKey('password', $this->Operator->validationErrors);
		
		$data['cpassword'] = '123456';
		$this->assertInternalType('array', $this->Operator->save($data, true));
		$this->assertTrue($this->Operator->find('count') == 2);
		
		# testa a edição sem senha
		$dataEdit = array(
			'Operator' => array(
				'id' => 2,
				'name' => 'plastic',
				'email' => 'andre@mktvirtual.com.br',
				'password' => ''
			)
		);
		
		$this->assertInternalType('array', $this->Operator->save($dataEdit));
	}
	
	/**
	 * Testa o envio da tarefa de receber o e-mail para criar a nova senha na fila
	 *
	 * @return void
	 */
	public function testSendResetPassword()
	{
		$this->assertTrue($this->Operator->sendResetPassword(1));
		
		# testa se o hash não for definido
		$this->Operator->id = 1;
		$this->Operator->saveField('hash', null, false);
		$this->assertFalse($this->Operator->sendResetPassword(1));
	}
	
	public function testGetTypes()
	{
		$this->Operator = $this->getMockForModel('Operator', array('_getAuthUser'));
		$this->Operator->staticExpects($this->any())->method('_getAuthUser')->with('type')->will($this->returnValue(1));
		$this->assertEqual($this->Operator->getTypes(), array(
			1 => 'Operador',
			2 => 'Administrador'
		));
	}
	
	public function tearDown()
	{
		unset($this->Operator);
		parent::tearDown();
	}
}