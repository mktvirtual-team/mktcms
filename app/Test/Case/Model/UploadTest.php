<?php
App::uses('Upload', 'Model');

/**
 * Upload Test Case
 *
 */
class UploadTest extends CakeTestCase
{

	public $fixtures = array(
		'app.upload'
	);

	public function setUp()
	{
		parent::setUp();
		$this->Upload = ClassRegistry::init('Upload');
		//$this->Upload->uploadSettings('file', 'path', WWW_ROOT . 'test_uploads');
		$this->Upload->Behaviors->unload('Upload.Upload');
		$this->Upload->Behaviors->load('Upload.Upload', array(
		    'file' => array(
				'maxSize' => 2048000,
				'rootDir' => ROOT,
				'path' => '{ROOT}{DS}test_uploads{DS}{model}{DS}{field}{DS}',
				'fields' => array(
					'dir' => 'id',
					'type' => '_type',
					'size' => '_size'
				),
				'deleteOnUpdate' => true
			)
		));
	}
	
	public function testInstance()
	{
		$this->assertIsA($this->Upload, 'Upload');
	}
	
	public function testValidation()
	{
		$data = array(
			'Upload' => array(
				'id' => 1,
				'file' => array(
					'name'  => 'cake.icon.png',
					'tmp_name' => '/Applications/MAMP/tmp/php/phpKuiUXc',
					'dir' => WWW_ROOT . '/img/cake.icon.png',
					'type'  => 'image/png',
					'size'  => 2049000,
					'error' => UPLOAD_ERR_OK,
				),
				'model' => 'Operator',
				'foreign_key' => 1,
				'created' => '2014-06-18 11:20:11',
				'modified' => '2014-06-18 11:20:11'
			)
		);
		
		$this->Upload->create();
		$this->Upload->set($data);
		
		// error validate file size
		$this->assertFalse($this->Upload->validates());
		
		$data['Upload']['file']['size'] = 8921;
		$this->Upload->clear();
		$this->Upload->set($data);
		
		// pass validate
		$this->assertTrue($this->Upload->validates());
		$this->assertEmpty($this->Upload->validationErrors);
		
		//$this->assertInternalType('array', $this->Upload->save($data, true));
		//$this->assertTrue($this->Upload->find('count') == 2);
	}
	
	public function testMultipleUpload()
	{
		$data = array(
			'Upload' => array(
				'id' => 1,
				'file' => array(
					array(
						'name'  => 'cake.icon.png',
						'tmp_name' => '/Applications/MAMP/tmp/php/phpKuiUXc',
						'dir' => WWW_ROOT . 'img/cake.icon.png',
						'type'  => 'image/png',
						'size'  => 2049000,
						'error' => UPLOAD_ERR_OK,
					),
					array(
						'name'  => 'cake.jpg',
						'tmp_name' => '/Applications/MAMP/tmp/php/phpKuiUXc',
						'dir' => WWW_ROOT . 'img/cake.icon.jpg',
						'type'  => 'image/jpeg',
						'size'  => 49000,
						'error' => UPLOAD_ERR_OK,
					)
				),
				'model' => 'Operator',
				'foreign_key' => 1,
				'created' => '2014-06-18 11:20:11',
				'modified' => '2014-06-18 11:20:11'
			)
		);
		
		$this->assertFalse($this->Upload->multipleUpload());
		
		$upload = $this->Upload->multipleUpload($data);
		
		$this->assertInternalType('array', $upload);
		
		$this->assertArrayHasKey('errors', $upload);
		
		$this->assertEqual("Arquivo deve ser menor que 2MB", $upload['errors'][0]['message']);
		
		$this->assertEqual("Erro ao salvar o arquivo", $upload['errors'][1]['message']);
	}
	
	/**
	 * tearDown method
	 *
	 * @return void
	 */
	public function tearDown()
	{
		unset($this->Upload);
		$uploadFolder = new Folder(ROOT . '/test_uploads');
		$uploadFolder->delete();
		parent::tearDown();
	}
}