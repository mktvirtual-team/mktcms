<?php
App::uses('Log', 'Model');

/**
 * Log Test Case
 *
 */
class LogTest extends CakeTestCase
{
	/**
	 * Fixtures
	 *
	 * @var array
	 */
	public $fixtures = array(
		'app.log',
		'app.operator'
	);

	/**
	 * setUp method
	 *
	 * @return void
	 */
	public function setUp()
	{
		parent::setUp();
		$this->Log = ClassRegistry::init('Log');
	}
	
	/**
	 * Testa as variáveis constantes
	 *
	 * @return void
	 */
	public function testConstsVariables()
	{
		$this->assertTrue(Log::ACTION_ADD === 1);
		$this->assertEquals(Log::$actions[Log::ACTION_ADD], 'Adicionou');
		$this->assertEquals(Log::$labels['who'], 'Quem');
		$this->assertEquals(Log::$label, $this->Log->name);
	}
	
	public function testInstance()
	{
		$this->assertIsA($this->Log, 'Log');
	}
	
	public function testAdd()
	{
		$data = array(
			'Log' => array(
				'id' => 2,
				'who' => 'Lorem ipsum dolor sit amet',
				'action' => 1,
				'model' => 'Lorem ipsum dolor sit amet',
				'model_id' => 1,
				'old_data' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
				'diff' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
				'user_id' => 1,
				'created' => '2014-06-18 11:20:21'
			)
		);
		
		$this->Log->create();
		$this->Log->set($data);
		$this->assertInternalType('array', $this->Log->save($data, true));
		$this->assertTrue($this->Log->find('count') == 2);
	}
	
	public function testChangesFunction()
	{
		$this->Operator = ClassRegistry::init('Operator');
		$data = $this->Operator->read(null, 1);
		$changes = Log::changes('Operator', json_encode($data['Operator']));
		
		$expected = 'id: 1, Tipo: 1, E-mail: admin@admin.com, Senha: 123456, Nome: Lorem ipsum dolor sit amet, Ativo: 1, hash: hash, created: 14/08/2014 11:43:10';
		$this->assertEqual($changes, $expected);
		
		$failChanges = Log::changes('Operator', false);
		$this->assertTrue(empty($failChanges));
	}
	
	/**
	 * tearDown method
	 *
	 * @return void
	 */
	public function tearDown()
	{
		unset($this->Log);
		parent::tearDown();
	}
}