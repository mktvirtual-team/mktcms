<?php
App::uses('Foo', 'Model');

/**
 * Foo Test Case
 *
 */
class FooTest extends CakeTestCase
{

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.foo',
		'app.bar'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp()
	{
		parent::setUp();
		$this->Foo = ClassRegistry::init('Foo');
	}

	public function testInstance()
	{
		$this->assertIsA($this->Foo, 'Foo');
	}
	
	public function testConstVariables()
	{
		$this->assertTrue(Foo::TYPE_1 === 1);
		$this->assertEquals(Foo::$types[Foo::TYPE_1], 'Tipo 1');
		$this->assertEquals(Foo::$labels['name'], 'Nome do Foo');
	}
	
	public function testFicture()
	{
		$expected = array(
			'Foo' => array(
				'id' => 1,
				'name' => 'Lorem ipsum dolor sit amet',
			)
		);
		$this->assertEquals($this->Foo->read(array('id', 'name'), 1), $expected);
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Foo);
		parent::tearDown();
	}

}