<?php

class WebTest extends PHPUnit_Extensions_Selenium2TestCase
{
	protected function setUp()
	{
		$this->setBrowser('chrome');
		$this->setBrowserUrl('http://localhost:8888/mktcms');
	}

	public function testPage()
	{
		$this->url('http://localhost:8888/mktcms');
		
		$this->timeouts()->implicitWait(5000);
		
		$this->assertEquals('Home', $this->title());
		
		$this->assertRegExp('/(CAKEPHP\ \-\ MKTCMS)/i', $this->source());
		sleep(1);
		
		$link = $this->byClassName('home');
		$this->assertEquals('home', $link->text());

		sleep(1);
		$link->click();
		
		$this->assertEquals('Home', $this->title());
		sleep(2);
		
		$this->timeouts()->implicitWait(5000);
	}
	
	/**
	 * Teste de tags (title, description e etcs)
	 *
	 * @return void
	 */
	# public function testTitle()
	# {
		# $this->timeouts()->implicitWait(10000);
		# $this->url('http://localhost:8888/mktcms');
		
		# verifica se a tag title existe
		# $this->assertRegExp('/(\<title[^>]*\>(.+)\<\/title\>)/i', $this->source(), 'Tag title não esta presente');
		
		# verifica se a tag description existe
		# $this->assertRegExp('/(\<meta\sname\=\"description\"\s(content\=\"(.+)\")\s?\/>)/i', $this->source(), 'Tag description não esta presente');
		
		# verifica se a tag h1,h2,h3 existem
		# $this->assertRegExp('/(\<h2[^>]*\>(.+)\<\/h2\>)/i', $this->source(), 'Tag H... não esta presente');
		
		# verifica se a tag meta robots existe
		/*
		$this->assertNotRegExp('/(\<meta\sname\=\"robots\"(\s(content\=\"(.+)\"))\s?\/?>)/i', $this->source(), 'Tag robots esta presente');
		*/
		# Verifica se existe o script para adicionar o ga.js
		# $this->assertRegExp('/GoogleAnalyticsObject/i', $this->source(), 'GA.JS não localizado');
		# $this->assertRegExp('/ga\(\'create\'\,\s?\'UA\-[^\-]*/i', $this->source(), 'GA não identificado');
		
		/*
		# verifica se existe a function ga
		$this->timeouts()->asyncScript(5000);
		$script = 'var callback = arguments[0]; return (typeof(ga) == "function") ? true : false;';
		$result = $this->execute(array(
			'script' => $script,
			'args'   => array()
		));
		$this->assertEquals(true, $result);
		*/
	# }
	
	public function testAdminLogin()
	{
		$this->url('http://localhost:8888/mktcms/admin');
		
		$this->timeouts()->implicitWait(5000);
		
		$this->assertEquals('[Title] | Painel de Controle', $this->title());
		sleep(1);
		$this->timeouts()->implicitWait(5000);
		
		$form = $this->byId('OperatorAdminLoginForm');
		$action = $form->attribute('action');
		sleep(1);
		$this->assertEquals('http://localhost:8888/mktcms/admin/login', $action);
		
		$this->byId('OperatorEmail')->clear();
		$this->byId('OperatorEmail')->value('mktvirtual@mktvirtual.com.br');
		$this->byId('OperatorPassword')->clear();
		$this->byId('OperatorPassword')->value('123456');
		sleep(1);
		
		$form->submit();
		
		sleep(1);
		
		$this->assertRegExp('/Home/i', $this->source());
		sleep(1);
	}
	
	/*
	public function testContact()
	{
		$this->url("http://localhost:8899/releases/contact");
		
		$form = $this->byId('ModelContactForm');
		$action = $form->attribute('action');
		sleep(2);
		$this->assertEquals( 'http://localhost:8899/releases/contact', $action);
		
		$this->byId('ModelName')->value('andre andrade');
		sleep(2);
		$form->submit();
		
		$this->assertEquals('Contato enviado!', $this->title());
		
		$this->assertRegExp('/Contato\senviado\!/i', $this->source());
		sleep(2);
	}*/
}