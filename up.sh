red=$(tput setaf 1)
green=$(tput setaf 2)
yellow=$(tput setaf 3)
white=$(tput setaf 7)

deploy=("" "[deploy: dev]" "[deploy: pro]" "[deploy: dev][deploy: pro]")

echo "$yellow"

echo "##################################"
echo "# Easy Git v1.0                  #"
echo "##################################"

echo "$red"

echo "Fazer deploy para quais ambientes?"

echo "$white"

echo "[0] - Não fazer deploy"
echo "[1] - Desenvolvimento [deploy: dev]"
echo "[2] - Produção [deploy: pro]"
echo "[3] - Todos os ambientes [deploy: dev][deploy: pro]"

read deploy_opt

# echo "${deploy[$deploy_opt]}"

echo "$red"

echo "Mensagem:$white"
read msg

git add .
git commit -m "$msg ${deploy[$deploy_opt]}"
git push

echo "$green"
