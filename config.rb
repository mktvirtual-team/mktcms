preferred_syntax = :sass
http_path = '/'
css_dir = 'app/webroot/css'
sass_dir = 'app/webroot/sass'
images_dir = 'app/webroot/img'
relative_assets = true
line_comments = true
# output_style = :compressed