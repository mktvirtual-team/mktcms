# MKTCMS
-------------

Starter CakePHP Project by Mkt Virtual

[![Mkt Virtual](http://www.mktvirtual.com.br/wp-content/themes/mktvirtual/imagens/sobre_nos/sobre_a_mktvirtual.png)](http://www.mtkvirtual.com.br)
[![CakePHP](http://cakephp.org/img/cake-logo.png)](http://www.cakephp.org)

### Install

Basic installation

```sh
$ git clone git@bitbucket.org:mktvirtual-team/mktcms.git projectName
$ cd projectName
$ rm -rf .git
$ ./app/Console/cake schema create
$ ./app/Console/cake seed
```

or

```sh
$ curl -kO https://bitbucket.org/mktvirtual-team/mktcms/raw/master/install-cms.sh && sh install-cms.sh
```

Run and install Grunt / NodeJS

```sh
$ git clone git://github.com/creationix/nvm.git ~/nvm && . ~/nvm/nvm.sh && nvm install v0.10.28
$ npm install
$ grunt
```
Install Dependencies with Composer

```sh
$ composer install
```

### Features

---------------

[![Composer](https://www.drupal.org/files/images/logo-composer-transparent.png)](http://getcomposer.org)
[![PHPUnit](http://blog.testingbot.com/wp-content/uploads/2012/02/Phpunit-Logo.gif)](http://phpunit.de)
[![Selenium](http://www.seleniumhq.org/images/selenium-logo.png)](http://www.seleniumhq.org/)
[![Jenkins](https://raw.githubusercontent.com/cbadke/chocolateypackages/master/jenkins/jenkins_logo.png)](http://jenkinsci.org/)

Some features like

* Compress and combine scripts
* Fingerprinted assets
* Jenkins integration
* LogData / LogError
* Google AuthLogin
* New bake template
* System queue

Run acceptance test with selenium

```sh
$ phpunit ./app/Test/WebTest.php
```

Run unit tests

```sh
$ ./app/Console/cake test app AllTests
```

Jenkins integration

```sh
$ ant
```
---------------

By [Mkt Virtual](http://www.mktvirtual.com.br)