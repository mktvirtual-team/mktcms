#!/bin/bash

red=$(tput setaf 1)
white=$(tput setaf 7)

# Lê os dados para acesso ao banco de dados
configDb() {
	echo "Digite o host do banco (Ex: localhost ou 127.0.0.1):"
	read host
	echo "Digite o usuario do banco (Ex: root):"
	read user
	echo "Digite a senha do banco (Ex: root):"
	read pass
	echo "Digite a porta do banco (Ex: 8889):"
	read port
	echo "Digite o nome do banco a ser criado: (Ex: $folder)"
	read base

	if [ "$port" == "" ]; then
		port = 3306
	fi
}

# cria um link simbolico para rodar o client do mysql e cria o banco de dados para o projeto
createDb() {
	# ln -s /Applications/MAMP/tmp/mysql/mysql.sock /tmp/mysql.sock
	echo "Criando o banco de dados..."
	mysql -h $host -u $user -p -P $port -e "create database $base"
}

# Configura o CakePHP para conectar ao banco
configCake() {
	echo "Configurando o cakephp para conectar ao banco..."
	sed "s/localhost/$host/g" app/Config/database.php.default > app/Config/database.php.host
	sed "s/user/$user/g" app/Config/database.php.host > app/Config/database.php.user
	sed "s/\=\>\ \'password\'/\=\>\ \'$pass\'/g"  app/Config/database.php.user > app/Config/database.php.pass
	sed "s/database_name/$base/g" app/Config/database.php.pass > app/Config/database.php.base
	sed "s/\/\// /g" app/Config/database.php.base > app/Config/database.php
	rm app/Config/database.php.host
	rm app/Config/database.php.user
	rm app/Config/database.php.pass
	rm app/Config/database.php.base
	#rm app/Config/database.php.default
}

importDb(){
	echo "Criando as tabelas do projeto..."
	./app/Console/cake schema create
}

seedDb(){
	echo "Populando as tabelas com seed..."
	./app/Console/cake seed
}

configRepo(){
	echo "Configurando o repositório"
	rm -rf .git
	initRepo
}

initRepo(){
	git init
	echo "Repositorio criado, adicionando arquivos no repositorio...!"
	git add .
	git commit -m 'Iniciando projeto'
	echo "Primeiro commit feito!"
	sleep 2
	echo "Digite a url do seu repositório online (caso já existe, senão apenas tecle enter):"
	read url
	if [ "$url" != "" ]; then
		git remote add origin $url
		echo "Realizando o push..."
		git push origin master
	fi
}

# Done!
congratulations() {
	echo "****************************************************************************"
	echo "Parabéns, seu projeto está pronto para uso!"
        echo "Para acessar o admin utilize as credenciais abaixo."
        echo "http://localhost:8888/$folder"
        echo "Para acessar o admin utilize as credenciais abaixo."
        echo "http://localhost:8888/$folder/admin"
	echo "E-mail:   mktvirtual@mktvirtual.com.br"
	echo "Senha:    123456"
	echo "Bom trabalho ;)"
	echo "****************************************************************************"

	echo "
....................................................................
...#####....####....####...##..##..######..######..#####....####....
...##..##..##..##..##......##..##..##........##....##..##..##..##...
...#####...##..##..##.###..##..##..####......##....#####...##..##...
...##......##..##..##..##..##..##..##........##....##..##..##..##...
...##.......####....####....####...######..######..##..##...####....
....................................................................
	"

}

init() {

	# Folder do project
	echo "Digite o nome da pasta do projeto:"

	read folder

	# Clonando o project em $folder
	echo "Clonando o projeto..."

	git clone git@bitbucket.org:mktvirtual-team/mktcms.git $folder
	clear

	# Atualizando os submodulos
	echo "Projeto clonado!!!!"
	cd $folder

	# Criando e importando o banco de dados
	echo "Deseja criar o banco para o projeto? (y/n)"
	read banco
	if [ "$banco" == "y" ]; then
		configDb
		createDb
		configCake
		importDb
		seedDb
		clear
		configRepo
		congratulations
	fi
}

####################################################
# Verifica se tem os commandos necessários no path #
####################################################
check(){

    is_ready="true"

    #Verifica mysql
    if ! type "git" > /dev/null; then
      echo "[ERRO] - Git não encontrado"
      is_ready="false"
    fi

    if ! type "mysql" > /dev/null; then
      echo "[ERRO] - Mysql não encontrado"
      is_ready="false"
    fi

}

clear

echo "$red"

echo "

        :::   :::   :::    ::: :::::::::::          ::::::::    :::   :::    ::::::::
      :+:+: :+:+:  :+:   :+:      :+:             :+:    :+:  :+:+: :+:+:  :+:    :+:
    +:+ +:+:+ +:+ +:+  +:+       +:+             +:+        +:+ +:+:+ +:+ +:+
   +#+  +:+  +#+ +#++:++        +#+             +#+        +#+  +:+  +#+ +#++:++#++
  +#+       +#+ +#+  +#+       +#+             +#+        +#+       +#+        +#+
 #+#       #+# #+#   #+#      #+#             #+#    #+# #+#       #+# #+#    #+#
###       ### ###    ###     ###              ########  ###       ###  ########


"

echo "$white"

check

if [ $is_ready == "true" ]; then
 init
else
 echo "Impossível continuar!!"
fi
