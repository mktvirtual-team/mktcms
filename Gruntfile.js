module.exports = function(grunt) {
	'use strict';

	require('time-grunt')(grunt);

	grunt.initConfig({

		settings: {
			url: 'http://localhost:8888/mktcms',
			bin: 'vendors/bin/',
			webroot: 'app/webroot/',
			css: '<%= settings.webroot %>css',
			js: '<%= settings.webroot %>js',
			sass: '<%= settings.webroot %>sass',
			img: '<%= settings.webroot %>img'
		},

		pkg: grunt.file.readJSON('package.json'),

		clean: ['<%= settings.webroot %>test/testem.tap', '<%= settings.webroot %>cache/css/*', '<%= settings.webroot %>cache/js/*'],

		jshint: {
			all: ['<%= settings.js %>/*.js'],
			options: grunt.file.readJSON('.jshintrc')
		},

		notify: {
			watch: {
				options: {
					title: grunt.option('n-title') || 'Task Complete',
					message: grunt.option('n-msg') || 'SASS finished running',
				}
			},
			plato: {
				options: {
					title: 'Task Plato',
					message: 'Plato metrics finished'
				}
			},
			imagemin: {
				options: {
					title: 'Task Imagemin',
					message: 'Images optimized'
				}
			},
			php: {
				options: {
					title: 'Task PHP',
					message: 'PHPMD, PHPCPD, PHPDCD, PHPLOC, PHPCS, PHPCB, PHPUNIT finished'
				}
			}
		},

		watch: {
			sass: {
				files: ['<%= settings.sass %>/**/*.{scss,sass}'],
				tasks: ['compass:dev', 'notify:watch']
			}
		},

		compass: {
			pro: {
				options: {
					force: true,
					//config: 'config.rb',
					outputStyle: 'compressed',
					cssDir: '<%= settings.css %>',
					sassDir: '<%= settings.sass %>'
				}
			},
			dev: {
				options: {
					force: true,
					//config: 'config.rb',
					outputStyle: 'nested',
					cssDir: '<%= settings.css %>',
					sassDir: '<%= settings.sass %>'
				}
			}
		},

		uncss: {
			dist: {
				options: {
					urls: [ '<%= settings.url %>' ],
					report: 'gzip'
				},
				files: {
					'<%= settings.css %>/app.clean.css':  ['*.php']
				}
			},
			fly: {
				options: {
					urls: [grunt.option('url')],
					report: 'gzip'
				},
				files: {
					'<%= settings.webroot %>theme/admin/css/<%= grunt.option(\'name\')%>.clean.css':  ['*.php']
				}
			}
		},

		pageres: {
			dist: {
				options: {
					url: grunt.option('url') || '<%= settings.url %>',
					sizes: ['1200x800', '800x600', '500x800'],
					dest: '<%= settings.webroot %>/prints',
					delay: 3
				}
			}
		},

		testem: {
			options: {
				launch_in_ci: ['PhantomJS']
			},
			'<%= settings.webroot %>test/testem.tap': ['<%= settings.webroot %>test/*.html']
		},

		"qunit-cov": {
			test: {
				minimum: 0.9,
				srcDir: '<%= settings.js %>',
				depDirs: ['<%= settings.webroot %>test'],
				outDir: '<%= settings.webroot %>dist/cov',
				testFiles: ['<%= settings.webroot %>test/*.html']
			}
		},

		plato: {
			options: {
				title: 'CakePHP CMS',
				jshint: grunt.file.readJSON('.jshintrc'),
				exclude: /js\/vendor/
			},
			dev: {
				files: {
					'<%= settings.webroot %>dist/metrics': [ '<%= settings.js %>/**/*.js' ]
				}
			}
		},

		imagemin: {
			dynamic: {
				options: {
					optimizationLevel: 7,
					progressive: true
				},
				files: [{
					expand: true,
					cwd: '<%= settings.img %>/',
					src: ['**/*.{png,jpg,gif}', '!imagecache/**/*.{jpg,png,gif}'],
					dest: '<%= settings.img %>/'
				}]
			}
		},

		shell: {
			options: {
				stderr: false
			},

			init: {
				command: 'mkdir -p <%= settings.webroot %>dist/code-browser <%= settings.webroot %>dist/metrics <%= settings.webroot %>dist/logs <%= settings.webroot %>dist/cov'
			},

			phploc: {
				command: 'php <%= settings.bin %>phploc --quiet --log-xml <%= settings.webroot %>dist/logs/<%= grunt.template.today("isoDateTime") %>.xml --exclude app/Config/ app/'
			},

			phpcs: {
				command: 'php <%= settings.bin %>phpcs --report=checkstyle --report-file=<%= settings.webroot %>dist/logs/checkstyle.xml --standard=Zend --extensions=php app/ || true'
			},

			phpcb: {
				command: 'php <%= settings.bin %>phpcb --log <%= settings.webroot %>dist/logs/ --source app --output <%= settings.webroot %>dist/code-browser/ --ignore <%= settings.webroot %>node_modules/'
			},

			phpunit: {
				command: 'php app/Console/cake.php test app AllTests --coverage-html <%= settings.webroot %>dist/coverage'
			},

			selenium: {
				command: 'java -jar ~/selenium-server-standalone-2.42.1.jar'
			},

			driver: {
				command: [
					'cd ~/Desktop/',
					'chromedriver'
				].join(' && ')
			},

			webtest: {
				command: 'php <%= settings.bin%>phpunit app/Test/WebTest.php'
			}
		},

		phpmd: {
			dev: {
				dir: 'app/'
			},
			options: {
				rulesets: 'codesize,unusedcode,naming',
				bin: '<%= settings.bin %>phpmd',
				reportFile: '<%= settings.webroot %>dist/logs/<%= grunt.template.today("isoDateTime") %>.xml'
			}
		},

		phpcpd: {
			dev: {
				dir: 'app/'
			},
			options: {
				quiet: true,
				exclude: 'app/Config/',
				bin: '<%= settings.bin %>phpcpd',
				reportFile: '<%= settings.webroot %>dist/logs/pmd-cpd-<%= grunt.template.today("isoDateTime") %>.xml',
				ignoreExitCode: true
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-compass');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-testem');
	grunt.loadNpmTasks('grunt-qunit-cov');
	grunt.loadNpmTasks('grunt-plato');
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-shell');
	grunt.loadNpmTasks('grunt-phpmd');
	grunt.loadNpmTasks('grunt-phpcpd');
	grunt.loadNpmTasks('grunt-notify');
	grunt.loadNpmTasks('grunt-uncss');
	grunt.loadNpmTasks('grunt-pageres');

	grunt.registerTask('default', ['watch']);
	grunt.registerTask('builder', ['jshint', 'clean', 'compass:pro', 'imagemin', 'notify:imagemin']);
	grunt.registerTask('php', ['shell:init', 'phpmd', 'phpcpd', 'shell:phploc', 'shell:phpcs', 'shell:phpcb', 'shell:phpunit', 'notify:php']);
	grunt.registerTask('jenkins', ['shell:init', 'jshint', 'testem', 'clean', 'qunit-cov', 'plato', 'notify:plato', 'phpmd', 'phpcpd', 'shell:phploc', 'shell:phpcs', 'shell:phpcb']);
};
